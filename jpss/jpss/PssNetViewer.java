package jpss;

import fr.esrf.tangoatk.core.Device;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.core.ISpectrumListener;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.widget.util.interlock.NetEditorListener;
import fr.esrf.tangoatk.widget.util.interlock.NetObject;
import fr.esrf.tangoatk.widget.util.interlock.NetEditor;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.Tango.DevFailed;

import javax.swing.*;
import java.io.IOException;
import java.util.Vector;
import java.awt.*;

/** An inner class to handle PSS interlock data */
class ItlkModel {

  final static int RT_MODE=1;  // Real time mode
  final static int LOG_MODE=2; // Logging mode

  INumberSpectrum model;     // Interlock spectrum
  Vector          objects;   // Vector of NetObject associated to this model
  int             mode;      // Monitoring mode
  int             logTime;   // Log timestamp when LOG_MODE -1 otherwise

  ItlkModel(INumberSpectrum m,Vector o) {
    model=m;
    objects=o;
    mode=RT_MODE;
    logTime = -1;
  }

}

/** A specific ATK viewer which handle PSS interlock diagram */
public class PssNetViewer extends PssNetEditor implements ISpectrumListener,NetEditorListener {

  // models
  private fr.esrf.tangoatk.core.AttributeList attList;
  private Vector<ItlkModel> itlkModels;
  private ErrorHistory errorDlg;
  private PssInterlockDlg itlkDlg = null;
  private PssInterlockHistoryDlg histDlg = null;
  private HDBDialog hdbDlg = null;

  private Color defaultBackground;

  /**
   * Contrust a PssNetViewer.
   * @param parent Parent frame
   */
  public PssNetViewer(JFrame parent) {

    super(parent);
    setEditable(false);

    // Global init ---------------------------------------------------------------------

    attList=new fr.esrf.tangoatk.core.AttributeList();
    errorDlg = new ErrorHistory();
    attList.addErrorListener(errorDlg);
    itlkModels = new Vector<ItlkModel>();

    addEditorListener(this);

    defaultBackground = getBackground();

  }

  // -----------------------------------------------------
  // NetEditor listener
  // -----------------------------------------------------
  public void valueChanged(NetEditor src) {}

  public void linkClicked(NetEditor src, NetObject obj, int childIdx, java.awt.event.MouseEvent e) {}

  public void objectClicked(NetEditor src, NetObject obj, java.awt.event.MouseEvent e) {

    if(obj.getUserType()!=PssNetEditor.ITLK_BUBBLE)
      return;

    if( getII(obj).configError!=null ) {
      JOptionPane.showMessageDialog(this,getII(obj).configError);
      return;
    }

    // Search model of corresponding ItlkObject object
    int i=0,j=0;
    boolean found=false;
    while(i<itlkModels.size() && !found) {
      j=0;
      Vector objs = getModelAt(i).objects;
      while(j<objs.size() && !found) {
        found = ( objs.get(j)==obj );
        if(!found) j++;
      }
      if(!found) i++;
    }

    if(found) {
      if(itlkDlg==null) itlkDlg = new PssInterlockDlg(getParentFrame());
      itlkDlg.showInterlockDialog((Device)getModelAt(i).model.getDevice(),obj);
    } else {
      String err = "Interlock object has a wrong device definition";
      String devName = obj.getExtendedParam("Device");
      if(devName.length()>0) err = "Failed to connect to " + devName + "/interlocks";
      JOptionPane.showMessageDialog(this,err);
    }

  }

  public void sizeChanged(NetEditor src,Dimension d) {}

  public void cancelCreate(NetEditor src) {
  }

  // ----------------------------------------------------------
  // Spectrum Listener
  // ----------------------------------------------------------

  public void errorChange(fr.esrf.tangoatk.core.ErrorEvent errorEvent) {

    INumberSpectrum m = (INumberSpectrum)errorEvent.getSource();
    int idx = findModel(m);
    if( idx!=-1 ) {
      if (getModelAt(idx).mode == ItlkModel.RT_MODE) {
        Vector objList = getModelAt(idx).objects;
        for (int i = 0; i < objList.size(); i++)
          setState((NetObject) objList.get(i), PssNetEditor.UNKNOWN_STATE);
        repaint();
      }
    }

  }

  public void stateChange(fr.esrf.tangoatk.core.AttributeStateEvent evt) {
  }

  synchronized public void spectrumChange(fr.esrf.tangoatk.core.NumberSpectrumEvent evt) {

    //System.out.println("Entering spectrum Change...");

    // Retrieve ItlkModel object attached to this interlock spectrum
    INumberSpectrum m = (INumberSpectrum)evt.getSource();
    int idx = findModel(m);

    if( idx!=-1 && evt.getValue()!=null && evt.getValue().length>0 ) {

      Vector objList = getModelAt(idx).objects;
      short[] itlkValues = null;
      int i;

      if (getModelAt(idx).mode != ItlkModel.LOG_MODE) {

        // Convert the attribute value to short
        itlkValues = new short[evt.getValue().length];
        for (i = 0; i < evt.getValue().length; i++)
          itlkValues[i] = (short) evt.getValue()[i];

        // Update the NetObject and repaint the NetEditor.
        for (i = 0; i < objList.size(); i++)
          setItlkState(itlkValues, (NetObject) objList.get(i));
        repaint();

      }

    }

  }

  // ----------------------------------------------------------

  /** Overload load file to check the root */
  public void loadFile(String fileName) throws IOException {

    super.loadFile(fileName);
    if( getRoot()==null ) {
      clearObjects();
      throw new IOException("No VCC found in this net file.");
    }

  }

  /** Clear all models belonging to this viewer */
  public void clearModel() {

    attList.stopRefresher();
    for(int i=0;i<attList.size();i++)
      ((INumberSpectrum)attList.get(i)).removeSpectrumListener(this);
    attList.removeErrorListener(errorDlg);
    attList.clear();
    itlkModels.clear();

  }

  /**
   * Returns inner models of this viewer.
   * Use for read only purpose
   * @return Array of ItlkModel
   */
  public Vector getModels() {
    return itlkModels;
  }

  /**
   * Sets the model of this viewer.
   * @param fileName Net filename
   * @throws IOException In case of failure
   */
  public void setModel(String fileName) throws IOException {

    int i;

    // clear old models
    clearModel();

    // Load the file
    loadFile(fileName);

    // Browse object and build models
    int sz = getNetObjectNumber();
    for(i=0;i<sz;i++) {
      if( getNetObjectAt(i).getUserType()==PssNetEditor.ITLK_BUBBLE )
        addInterlock(getNetObjectAt(i));
    }

    attList.setRefreshInterval(2000);
    attList.startRefresher();

  }

  /** Reset all PSS crate within the loaded scheme */
  public void resetPSS() {

    if(itlkModels.size()==0 ) {
      if( getFileName().length()>0) {
        JOptionPane.showMessageDialog(this,"No connection to hardware.","Initialisation error",JOptionPane.ERROR_MESSAGE);
      } else {
        JOptionPane.showMessageDialog(this,"No diagram loaded.","Initialisation error",JOptionPane.ERROR_MESSAGE);
      }
    }

    for(int i=0;i<itlkModels.size();i++) {
      DeviceProxy ds = (Device)getModelAt(i).model.getDevice();
      try {
        ds.command_inout("Reset");
      } catch (DevFailed e) {
        ErrorPane.showErrorMessage(this,ds.get_name(),e);
      }
    }

  }

  /** Restore normal monitoring mode */
  public void restoreNormalMode() {

    for(int i=0;i<itlkModels.size();i++) {
      getModelAt(i).mode = ItlkModel.RT_MODE;
      getModelAt(i).logTime = -1;
    }
    setBackground(defaultBackground);

  }

  /** Show the error dialog */
  public void showErrors() {
    ATKGraphicsUtils.centerFrameOnScreen(errorDlg);
    errorDlg.setVisible(true);
  }

  /**
   * Set up history viewing (from memory)
   * @param idx Model index
   * @return history time (-1 when failed or canceled).
   */
  synchronized void showMemHistory(int idx) {

    if(histDlg==null) histDlg = new PssInterlockHistoryDlg(getParentFrame());
    int tLog = histDlg.getHistoryTime((Device)getModelAt(idx).model.getDevice());
    if (tLog != -1) {
      getModelAt(idx).mode = ItlkModel.LOG_MODE;
      getModelAt(idx).logTime = tLog;
      DeviceProxy ds = (Device)getModelAt(idx).model.getDevice();
      try {

        DeviceData argin = new DeviceData();
        argin.insert(tLog);
        DeviceData argout = ds.command_inout("ReadInterlockHistory",argin);

        short[] itlkValues = argout.extractShortArray();
        Vector objList = getModelAt(idx).objects;
        for (int i = 0; i < objList.size(); i++)
          setItlkState(itlkValues, (NetObject) objList.get(i));

        setBackground(new Color(200,230,200));
        repaint();

      } catch (DevFailed e) {
        ErrorPane.showErrorMessage(this,ds.get_name(),e);
        restoreNormalMode();
      }

    }

  }

  /**
   * Set up history viewing (from HDB)
   * @param idx Model index
   * @return history time (-1 when failed or canceled).
   */
  synchronized void showHDBHistory(int idx) {

    if(hdbDlg==null) hdbDlg = new HDBDialog(getParentFrame());
    hdbDlg.setDevName(getModelAt(idx).model.getDevice().getName());
    ATKGraphicsUtils.centerDialog(hdbDlg);
    hdbDlg.setVisible(true);
    if (hdbDlg.selectOK) {

      getModelAt(idx).mode = ItlkModel.LOG_MODE;
      getModelAt(idx).logTime = hdbDlg.retTimeStamp;
      DeviceProxy ds = (Device)getModelAt(idx).model.getDevice();
      Vector objList = getModelAt(idx).objects;
      for (int i = 0; i < objList.size(); i++)
        setItlkState(hdbDlg.retItlk, (NetObject) objList.get(i));

      setBackground(new Color(200,230,200));
      repaint();

    }


  }

  // Build a relation between ItlkObject and ATK models (INumerSpectrum)
  private void addInterlock(NetObject o) {

    setState(o, PssNetEditor.UNKNOWN_STATE);

    if (checkRelayConfig(o)) {

      Vector v = null;
      String attName = getII(o).devName + "/interlocks";

      try {

        INumberSpectrum i = (INumberSpectrum) attList.add(attName);
        int idx = findModel(i);
        if (idx < 0) {
          // New model
          v = new Vector();
          ItlkModel m = new ItlkModel(i, v);
          i.addSpectrumListener(this);
          itlkModels.add(m);
        } else {
          v = getModelAt(idx).objects;
        }
        v.add(o);

      } catch (ConnectionException e) {
        System.out.println("Importing " + attName + " failed :" + e.getErrors()[0].desc);
      }

    }

  }

  // Compute the PSS bubble state (depends on a couple of interlock)
  private void setItlkState(short[] itlkValues,NetObject o) {

    // Extract bits
    int bMask = 3 << getII(o).bitNumber;
    short v = itlkValues[getII(o).wordAddress];
    v = (short)((v & bMask) >> getII(o).bitNumber);

    // Apply state
    switch(v) {
      case 0:
        setState(o,PssNetEditor.OPEN_STATE);
        setRelayState(o,PssNetEditor.OPEN_STATE,PssNetEditor.OPEN_STATE);
        break;
      case 1:
        setState(o,PssNetEditor.ALARM_STATE);
        setRelayState(o,PssNetEditor.CLOSE_STATE,PssNetEditor.OPEN_STATE);
        break;
      case 2:
        setState(o,PssNetEditor.ALARM_STATE);
        setRelayState(o,PssNetEditor.OPEN_STATE,PssNetEditor.CLOSE_STATE);
        break;
      case 3:
        setState(o,PssNetEditor.CLOSE_STATE);
        setRelayState(o,PssNetEditor.CLOSE_STATE,PssNetEditor.CLOSE_STATE);
        break;
    }

  }

  public String computeTabTitle(String fName) {

    boolean liveMode = true;

    StringBuffer title = new StringBuffer();
    for(int i=0;i<itlkModels.size();i++) {
      title.append("[");
      ItlkModel m = getModelAt(i);
      liveMode = liveMode && m.mode==ItlkModel.RT_MODE;
      title.append(m.model.getDevice().getName());
      title.append("@");
      int tLog;
      if(m.mode== ItlkModel.RT_MODE) {
        tLog = (int)(System.currentTimeMillis() / 1000);
      } else {
        tLog = m.logTime;
      }
      title.append(PssInterlockHistoryDlg.formatTimeValue(tLog));
      title.append("]");
    }

    if(liveMode) {
      return fName;
    } else {
      return fName + " " + title.toString();
    }

  }

  // Return the model at the specified position
  private ItlkModel getModelAt(int i) {
    return itlkModels.get(i);
  }

  // Find the model in the interlock array
  private int findModel(INumberSpectrum m) {
    int i=0;
    boolean found=false;
    while(i<itlkModels.size() && !found) {
      found = ( m==getModelAt(i).model );
      if(!found) i++;
    }
    if( found )
      return i;
    else
      return -1;
  }


}


