package jpss;

/** Class to handle PSS interlock specific data */
public class ItlkInfo {

  String  devName=null;      // The Tango device name
  int     wordAddress=-1;    // WORD index in the returned interlock array
  int     bitNumber=-1;      // Bit index (0to15)
  int     state;             // bubble state
  int     stateA;            // relayA state
  int     stateB;            // relatB state
  int     nbHit;             // mark for graph browsing
  int     storedResult;      // Temporary result for node with multiple input
  String  configError=null;  // configuration error

}
