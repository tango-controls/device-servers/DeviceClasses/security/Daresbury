package jpss;

/**
 * Multiline cell renderer
 */

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class MultiLineCellRenderer extends JTextArea implements TableCellRenderer {

  public MultiLineCellRenderer() {

    setEditable(false);
    setLineWrap(false);
    setWrapStyleWord(false);

  }

  public Component getTableCellRendererComponent(JTable table,Object value,
												boolean isSelected, boolean hasFocus, int row, int column) {

    if (value instanceof String) {
      updateRow(table,row);
      setText((String)value);
    }

    return this;

  }

  // This method determines the height in pixel of a cell given the text it contains
  private int cellHeight(JTable table,int row, int col) {

    Object val = table.getModel().getValueAt(row, col);
    if( val instanceof String ) {

      setText((String)val);
      return getPreferredSize().height + 1;

    }

    return 0;

  }

  void updateRow(JTable table,int row) {

    int maxHeight = table.getRowHeight();
    for (int j = 0; j < table.getColumnCount(); j++) {
      int ch;
      if ((ch = cellHeight(table, row, j)) > maxHeight) {
        maxHeight = ch;
      }
    }
    table.setRowHeight(row, maxHeight);

  }

}

