package jpss;

/**
 * SVN log info
 */
public class SVNStatusInfo {

  public String fileName;
  public long   activeRev;
  public long   lastChangedRev;
  public int    status;

}
