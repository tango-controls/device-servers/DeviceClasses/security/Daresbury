package jpss;

import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.SVNRevision;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Vector;

/**
 * A dialog to display history release
 */
public class RevisionDialog extends JFrame implements ActionListener {

  private JPanel innerPanel;

  private JScrollPane revScroll;
  private JTable revTable;
  private DefaultTableModel revModel;
  private String colName[];
  private Object[][] revInfo;

  private JPanel buttonPanel;
  private JButton commitBtn;
  private JButton revertBtn;
  private JButton retrieveBtn;
  private JButton dismissBtn;

  private JLabel infoLabel;

  private Vector<SVNLogInfo> logs;

  private int selected;

  private File logFile;

  private SVNHelper svnHelper;
  private Reloadable loader;

  RevisionDialog(boolean editMode,SVNHelper svnHelper,Reloadable loader) {

    this.svnHelper = svnHelper;
    this.loader = loader;

    innerPanel = new JPanel();
    innerPanel.setLayout(new BorderLayout());

    setContentPane(innerPanel);

    buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

    if( editMode ) {

      commitBtn = new JButton("Commit change");
      commitBtn.addActionListener(this);
      buttonPanel.add(commitBtn);

      revertBtn = new JButton("Revert to selected revision");
      revertBtn.addActionListener(this);
      buttonPanel.add(revertBtn);

    }

    retrieveBtn = new JButton("Retrieve selected revision");
    retrieveBtn.addActionListener(this);
    buttonPanel.add(retrieveBtn);

    dismissBtn = new JButton("Dismiss");
    dismissBtn.addActionListener(this);
    buttonPanel.add(dismissBtn);

    innerPanel.add(buttonPanel,BorderLayout.SOUTH);

    // -- Revision history table -------------------------------

    revModel = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        if( columnIndex==3 ) {
          return Boolean.class;
        } else {
          return String.class;
        }
      }

      public boolean isCellEditable(int row, int column) {
          return (column==3);
      }

      public void setValueAt(Object aValue, int row, int column) {

        for(int i=0;i<getRowCount();i++) {
          super.setValueAt( i==row , i , column );
        }
        fireTableDataChanged();
        selected = row;

      }

    };

    colName = new String[4];
    colName[0] = "Rev number";
    colName[1] = "Date";
    colName[2] = "Log message";
    colName[3] = "Selection";

    revTable = new JTable(revModel);
    MultiLineCellRenderer cellRenderer = new MultiLineCellRenderer();
    revTable.setDefaultRenderer(String.class,cellRenderer);
    revScroll = new JScrollPane(revTable);
    innerPanel.add(revScroll,BorderLayout.NORTH);
    revInfo = new Object[0][4];
    revModel.setDataVector(revInfo,colName);

    infoLabel = new JLabel(" ");
    innerPanel.add(infoLabel,BorderLayout.CENTER);

    innerPanel.setPreferredSize(new Dimension(640,480));
    ATKGraphicsUtils.centerFrameOnScreen(this);

  }

  public void refresh() throws SVNException {

    setFilename(logFile.getPath());

  }

  public void setFilename(String fileName) throws SVNException {

    logFile = new File(fileName);
    setTitle("Revision history of " + logFile.getName());

    logs = svnHelper.log(logFile);
    long activeRev = svnHelper.getActiveRevision(logFile);
    if (activeRev > logs.lastElement().revNumber) activeRev = logs.lastElement().revNumber;

    revInfo = new Object[logs.size()][4];
    for (int i = 0; i < logs.size(); i++) {
      revInfo[i][0] = Long.toString(logs.get(i).revNumber);
      revInfo[i][1] = logs.get(i).date;
      revInfo[i][2] = logs.get(i).logMessage;
      if (activeRev == logs.get(i).revNumber) {
        selected = i;
        revInfo[i][3] = true;
      } else {
        revInfo[i][3] = false;
      }
    }

    revModel.setDataVector(revInfo, colName);
    revTable.getColumnModel().getColumn(1).setPreferredWidth(100);
    revTable.getColumnModel().getColumn(2).setPreferredWidth(250);

    int status = svnHelper.status(logFile);

    infoLabel.setText("Current working revision: " + activeRev + " [" + SVNHelper.StatusToStr(status) + "]");

  }

  public void actionPerformed(ActionEvent evt) {

    Object src = evt.getSource();

    if( src==dismissBtn ) {
      super.setVisible(false);
    } else if ( src==retrieveBtn ) {

      long rev = logs.get(selected).revNumber;

      int ans = JOptionPane.showConfirmDialog(this,"Do you want to update to revision " + rev + " ?","SVN",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
      if( ans==JOptionPane.YES_OPTION ) {

        // remove old file (to avoid conflict)
        logFile.delete();

        // update to selected revision
        try {
          svnHelper.update(logFile,SVNRevision.create(rev));
          refresh();
          loader.reload();
        } catch(SVNException e) {
          JOptionPane.showMessageDialog(this,e.getMessage(),"SVN Error",JOptionPane.ERROR_MESSAGE);
        }
      }

    } else if ( src==revertBtn ) {

      if( selected==logs.size()-1 ) {
        JOptionPane.showMessageDialog(this,"Cannot revert to head revision","Error",JOptionPane.ERROR_MESSAGE);
        return;
      }

      long rev = logs.get(selected).revNumber;

      int ans = JOptionPane.showConfirmDialog(this,"Do you want to revert to revision " + rev + " ?","SVN",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
      if( ans==JOptionPane.YES_OPTION ) {

        // remove old file (to avoid conflict)
        logFile.delete();

        // revert to selected revision
        try {

          svnHelper.update(logFile,SVNRevision.create(rev));
          File tmpFile = new File(logFile.getPath() + ".tmp");
          logFile.renameTo(tmpFile);
          svnHelper.update(logFile,SVNRevision.HEAD);
          logFile.delete();
          tmpFile.renameTo(logFile);
          svnHelper.commit(logFile,"Revert to revision " + rev);
          refresh();
          loader.reload();

        } catch(SVNException e) {
          JOptionPane.showMessageDialog(this,e.getMessage(),"SVN Error",JOptionPane.ERROR_MESSAGE);
        }


      }

    } else if (src==commitBtn) {

      // Commit
      String comMessage = MessageEditor.editMessage(this,"Committing " + logFile.getName() + " : Enter log message","No message");
      if( comMessage!=null ) {
        try {
          SVNCommitInfo comInfo = svnHelper.commit(logFile,comMessage);
          if( comInfo == SVNCommitInfo.NULL ) {
            JOptionPane.showMessageDialog(this,"Nothing to commit","SVN Error",JOptionPane.ERROR_MESSAGE);
          }
          refresh();
        } catch(SVNException e) {
          JOptionPane.showMessageDialog(this,e.getMessage(),"SVN Error",JOptionPane.ERROR_MESSAGE);
        }
      }

    }

  }

}
