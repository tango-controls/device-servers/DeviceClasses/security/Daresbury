package jpss;

/**
 * SVN log info
 */
public class SVNLogInfo {

  public String logMessage;
  public long   revNumber;
  public String date;

}
