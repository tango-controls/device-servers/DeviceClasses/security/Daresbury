package jpss;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import org.tango.jhdb.Hdb;
import org.tango.jhdb.HdbFailed;
import org.tango.jhdb.HdbReader;
import org.tango.jhdb.data.HdbData;
import org.tango.jhdb.data.HdbDataSet;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import static jpss.PssConst.TANGO_HOST;

public class HDBFrame extends JFrame implements ActionListener {

  public final static SimpleDateFormat dfr = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
  public final static int MAX_MODULES = 5000;

  private JTable nameTable;
  private DefaultTableModel nameTableModel;
  private String colNames[];
  private Hdb hdb;

  private JDialog dataDlg;
  private JTable dataTable;
  private DefaultTableModel dataTableModel;

  private JButton dismissBtn;
  private JButton performSearchBtn;

  private JButton selectAllBtn;
  private JButton selectNoneBtn;
  private JButton selectBMBtn;
  private JButton selectIDBtn;
  private JButton selectMACHBtn;
  private JButton selectNoneModBtn;
  private JButton selectAllModBtn;
  private JComboBox timeCombo;
  private JLabel startDateLabel;
  private JTextField startDateText;
  private JLabel stopDateLabel;
  private JTextField stopDateText;
  private JCheckBox sortByDateCheck;

  private JCheckBox[] moduleCheck;

  class HDBLine {

    HDBLine(long date) {
      this.date = date;
      for(int i=0;i<MAX_MODULES;i++)
        value[i] = -1;
    }

    long date;
    long[] value = new long[MAX_MODULES];

  }

  Vector<HDBLine> hdbData = new Vector<>();
  Vector<String> moduleNames = new Vector<>();

  public HDBFrame() {

    JPanel innerPanel = new JPanel();
    innerPanel.setLayout(new BorderLayout());

    // Table model
    nameTableModel = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        if( columnIndex==1 ) {
          return Boolean.class;
        } else {
          return String.class;
        }
      }
      public boolean isCellEditable(int row, int column) {
        return (column==1);
      }
      public void setValueAt(Object aValue, int row, int column) {
        super.setValueAt( aValue , row , column );
        fireTableDataChanged();
      }

    };

    colNames = new String[2];
    colNames[0] = "Device name to query";
    colNames[1] = "Include in HDB query";

    // Table initialisation
    nameTable = new JTable(nameTableModel);
    nameTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    nameTable.setRowSelectionAllowed(true);

    fillName();

    JScrollPane scrollPane = new JScrollPane(nameTable);
    innerPanel.add(scrollPane, BorderLayout.CENTER);

    JPanel configPanel = new JPanel();
    configPanel.setLayout(null);
    configPanel.setPreferredSize(new Dimension(1000, 180));
    configPanel.setBorder(PssInterlockDlg.createTitleBorder("HDB request"));
    innerPanel.add(configPanel, BorderLayout.NORTH);

    timeCombo = new JComboBox();
    timeCombo.addItem("Last 4 Hours");
    timeCombo.addItem("Last 8 Hours");
    timeCombo.addItem("Last 24 Hours");
    timeCombo.addItem("Last 3 days");
    timeCombo.addItem("Last week");
    timeCombo.setBounds(10, 20, 300, 25);
    timeCombo.addActionListener(this);
    configPanel.add(timeCombo);

    startDateLabel = new JLabel("Start date");
    startDateLabel.setBounds(10, 50, 100, 25);
    configPanel.add(startDateLabel);
    startDateText = new JTextField();
    startDateText.setEditable(true);
    startDateText.setBounds(110, 50, 200, 25);
    configPanel.add(startDateText);

    stopDateLabel = new JLabel("Stop date");
    stopDateLabel.setBounds(10, 80, 100, 25);
    configPanel.add(stopDateLabel);
    stopDateText = new JTextField();
    stopDateText.setEditable(true);
    stopDateText.setBounds(110, 80, 200, 25);
    configPanel.add(stopDateText);
    innerPanel.add(configPanel,BorderLayout.NORTH);

    timeCombo.setSelectedIndex(0);

    selectAllBtn = new JButton("Select All");
    selectAllBtn.addActionListener(this);
    selectAllBtn.setBounds(320, 20, 150, 25);
    configPanel.add(selectAllBtn);

    selectNoneBtn = new JButton("Select None");
    selectNoneBtn.addActionListener(this);
    selectNoneBtn.setBounds(320, 50, 150, 25);
    configPanel.add(selectNoneBtn);

    selectIDBtn = new JButton("Select ID");
    selectIDBtn.addActionListener(this);
    selectIDBtn.setBounds(320, 80, 150, 25);
    configPanel.add(selectIDBtn);

    selectBMBtn = new JButton("Select BM");
    selectBMBtn.addActionListener(this);
    selectBMBtn.setBounds(475, 20, 150, 25);
    configPanel.add(selectBMBtn);

    selectMACHBtn = new JButton("Select Mach");
    selectMACHBtn.addActionListener(this);
    selectMACHBtn.setBounds(475, 50, 150, 25);
    configPanel.add(selectMACHBtn);

    sortByDateCheck = new JCheckBox("Sort by date");
    sortByDateCheck.setSelected(false);
    sortByDateCheck.setBounds(475,80,150,25);
    configPanel.add(sortByDateCheck);

    moduleCheck = new JCheckBox[22];
    for(int m=0;m<22;m++) {
      moduleCheck[m] = new JCheckBox("M"+(m+1));
      moduleCheck[m].setSelected(true);
      int col = m%11;
      int row = m/11;
      moduleCheck[m].setBounds(5 + col*70,110 + row*30,70,25);
      configPanel.add(moduleCheck[m]);
    }

    selectNoneModBtn = new JButton("None");
    selectNoneModBtn.setBounds(790,110,100, 25);
    selectNoneModBtn.addActionListener(this);
    configPanel.add(selectNoneModBtn);

    selectAllModBtn = new JButton("All");
    selectAllModBtn.setBounds(790,140,100, 25);
    selectAllModBtn.addActionListener(this);
    configPanel.add(selectAllModBtn);


    JPanel buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    innerPanel.add(buttonPanel, BorderLayout.SOUTH);


    performSearchBtn = new JButton("Perform search");
    performSearchBtn.addActionListener(this);
    buttonPanel.add(performSearchBtn);

    dismissBtn = new JButton("Dismiss");
    dismissBtn.addActionListener(this);
    buttonPanel.add(dismissBtn);

    setContentPane(innerPanel);

    setTitle("HDB PSS history " + PssConst.APP_RELEASE);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    pack();
    ATKGraphicsUtils.centerFrameOnScreen(this);


    // Data display
    dataDlg = new JDialog();
    JPanel innerDataPanel = new JPanel();
    innerDataPanel.setLayout(new BorderLayout());
    innerDataPanel.setPreferredSize(new Dimension(1024,768));

    dataTableModel = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        if(columnIndex==0) return String.class;
        else return Long.class;
      }
      public boolean isCellEditable(int row, int column) {
        return false;
      }
      public void setValueAt(Object aValue, int row, int column) {}

    };


    dataTable = new JTable(dataTableModel);
    ItlkCellRenderer dataCellRenderer = new ItlkCellRenderer();
    dataTable.setDefaultRenderer(Long.class,dataCellRenderer);

    dataTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    dataTable.setRowSelectionAllowed(true);
    dataTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

    JScrollPane dataScrollPane = new JScrollPane(dataTable,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    innerDataPanel.add(dataScrollPane, BorderLayout.CENTER);

    dataDlg.setContentPane(innerDataPanel);

  }

  private void fillName() {

    ArrayList<String> hdbNames = new ArrayList<>();
    ArrayList<String> pssNames = new ArrayList<>();

    // Connect to HDB++
    hdb = new Hdb();
    try {
      //export HDB_NAME=hdb
      //export HDB_TYPE=postgresql
      //export HDB_USER=hdb_java_reporter
      //export HDB_PASSWORD=hdbpp
      //export HDB_POSTGRESQL_HOST=hdb-services
      //export HDB_POSTGRESQL_PORT=5001

      // All the above environment variables should be set inside the shell to be able to connect
      // Connect to HDB ++ using system variables
      hdb.connect();
      System.out.println("HDB connection Ok: " + hdb.getReader().getInfo());

      // Retrieve list of stored attributes
      String[] names = hdb.getReader().getAttributeList();
      for (String name : names) hdbNames.add(name.toLowerCase());

    } catch (HdbFailed ex) {
      JOptionPane.showMessageDialog(null, ex.getMessage(), "HDB Error", JOptionPane.ERROR_MESSAGE);
      return;
    }


    // Get list of PSS device and check that they are archived in HDB

    try {

      Database db = ApiUtil.get_db_obj();
      String[] list = db.get_device_exported_for_class("Daresbury");
      for(String name:list) {
        String attName = TANGO_HOST + name + "/interlocks";
        if(hdbNames.contains(attName))
          pssNames.add(name);
      }

    } catch (DevFailed e) {

      JOptionPane.showMessageDialog(getParent(), e.errors[0].desc, "Tango Error", JOptionPane.ERROR_MESSAGE);

    }

    Object[][] prop = new Object[pssNames.size()][2];
    for(int i=0;i<pssNames.size();i++) {
      prop[i][0] = pssNames.get(i);
      prop[i][1] = true;
    }
    nameTableModel.setDataVector(prop, colNames);
    nameTable.getColumnModel().getColumn(0).setPreferredWidth(300);
    nameTable.validate();

  }

  private void selectPattern(String patterns) {

    Vector prop = nameTableModel.getDataVector();

    if(patterns.equalsIgnoreCase("all")) {
      for (int i = 0; i < prop.size(); i++)
        nameTableModel.setValueAt(true , i, 1);
    } else if(patterns.equalsIgnoreCase("none")) {
      for (int i = 0; i < prop.size(); i++)
        nameTableModel.setValueAt(false , i, 1);
    } else {

      String[] fields = patterns.split(",");

      for (int i = 0; i < prop.size(); i++) {
        Vector row = (Vector) prop.get(i);
        String name = (String) row.get(0);
        boolean ok = false;
        for (int j = 0; j < fields.length; j++)
          ok = ok | name.contains(fields[j]);
        nameTableModel.setValueAt(ok, i, 1);
      }

    }

    nameTableModel.fireTableDataChanged();

  }

  private void addHDBData(long date, int colIdx, long value, int startIdx) {

    boolean found = false;
    boolean addData = true;
    int i = startIdx;

    // Search insertion position
    while (i < hdbData.size() && !found) {
      found = date <= hdbData.get(i).date;
      if (!found) i++;
    }

    // Remove duplicate values
    if (i > 0) {
      // Go upward up to find data
      int j = i - 1;
      boolean validDataFound = false;
      while(j>=0 && !validDataFound) {
        validDataFound = hdbData.get(j).value[colIdx]!=-1;
        if(!validDataFound) j--;
      }
      addData = !validDataFound || hdbData.get(j).value[colIdx] != value;
    }

    // Add new item
    if (addData) {

      if( i>=hdbData.size() || hdbData.get(i).date!=date ) {

        // Add a new row
        HDBLine it = new HDBLine(date);
        if (!found) {
          hdbData.add(it);
        } else {
          hdbData.add(i, it);
        }

      }

      hdbData.get(i).value[colIdx] = value;

    }

  }

  private String shorten(String attName) {
    String[] fields = attName.split("/");
    return fields[fields.length-2].replace("crate","CR");
  }

  private boolean isConstant(Vector<long[]> array,int idx) {

    int size = array.size();
    if(size==0) return true;
    if(!moduleCheck[idx].isSelected()) return true;

    long fValue = array.get(0)[idx];
    int i = 1;
    boolean equal = true;
    while(i<size && equal) {
      equal = array.get(i)[idx] == fValue;
      i++;
    }
    return equal;

  }

  private void HDBSearch() {

    // Retrieve attribute list
    ArrayList<String> attToSearch = new ArrayList<String>();
    Vector prop = nameTableModel.getDataVector();

    for (Object o : prop) {
      Vector row = (Vector) o;
      String name = (String) row.get(0);
      boolean use = (Boolean) row.get(1);
      if (use) attToSearch.add(TANGO_HOST + name + "/interlocks");
    }

    String[] attList = new String[attToSearch.size()];
    for(int i=0;i<attToSearch.size();i++)
      attList[i] = attToSearch.get(i);

    // Check parameters
    String d1 = startDateText.getText();
    java.util.Date d;
    try {
      d = dfr.parse(d1);
    } catch (java.text.ParseException e1) {
      JOptionPane.showMessageDialog(null, "Invalid start date format\nUse dd/mm/yyyy hh:mm:ss",
              "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    String d2 = stopDateText.getText();
    try {
      d = dfr.parse(d2);
    } catch (java.text.ParseException e2) {
      JOptionPane.showMessageDialog(null, "Invalid stop date format\nUse dd/mm/yyyy hh:mm:ss",
              "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    hdbData.clear();
    moduleNames.clear();

    HdbDataSet[] data;

    try {
      data = hdb.getReader().getData(attList, startDateText.getText(), stopDateText.getText(), HdbReader.ExtractMode.MODE_NORMAL.ordinal());
    } catch (HdbFailed ex) {
      JOptionPane.showMessageDialog(getParent(), ex.getMessage(), "HDB Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    // Extract wanted data
    for(int i=0;i<data.length;i++) {

      // Get module data
      int dataSize = data[i].size();
      Vector<long[]> moduleHist = new Vector<>();
      for (int j = 0; j < dataSize; j++) {
        try {
          long[] moduleData = data[i].get(j).getValueAsLongArray();
          if( moduleData.length==22 )
            moduleHist.add(moduleData);
          else
            System.out.println("Error: " + attList[i] + " has " + moduleData.length + " items, (22 expected)");
        } catch (HdbFailed ex) {
          System.out.println("HDB Error: " + attList[i] + ": " +   ex.getMessage());
        }
      }

      // Extract module that has variations
      int startIndex = sortByDateCheck.isSelected()?0:hdbData.size();
      for(int m=0;m<22;m++) {
        if( !isConstant(moduleHist,m)) {
          // A new column
          moduleNames.add( shorten(attList[i]) + " M" + (m+1));
          for (int j = 0; j < moduleHist.size(); j++) {
            long date = data[i].get(j).getDataTime();
            addHDBData(date,moduleNames.size()-1,moduleHist.get(j)[m],startIndex);
          }
        }
      }

    }

    if (hdbData.size() == 0) {

      JOptionPane.showMessageDialog(null, "No data found", "HDB Error", JOptionPane.ERROR_MESSAGE);

    } else {

      // Build the table
      Object[][] vals = new Object[hdbData.size()][moduleNames.size()+1];
      for (int i = 0; i < hdbData.size(); i++) {
        Date dt = new java.util.Date(hdbData.get(i).date / 1000);
        vals[i][0] = dfr.format(dt) + "." + String.format("%03d",hdbData.get(i).date%1000);
        for (int j = 0; j < moduleNames.size(); j++)
            vals[i][j + 1] = hdbData.get(i).value[j];
      }

      String[] colName = new String[moduleNames.size()+1];
      colName[0] = "Date";
      for(int i=0;i<moduleNames.size();i++) colName[i+1] = moduleNames.get(i);

      dataTableModel.setDataVector(vals, colName);
      dataTable.getColumnModel().getColumn(0).setPreferredWidth(200);
      for(int i=0;i<moduleNames.size();i++)
        dataTable.getColumnModel().getColumn(i+1).setPreferredWidth(120);
      dataTable.validate();
      if(!dataDlg.isVisible()) {
        ATKGraphicsUtils.centerDialog(dataDlg);
        dataDlg.setVisible(true);
      }

    }

  }


  @Override
  public void actionPerformed(ActionEvent e) {
    Object src = e.getSource();
    if( src==dismissBtn ) {
      System.exit(0);
    } else if ( src==selectAllBtn ) {
      selectPattern("all");
    } else if ( src==selectNoneBtn ) {
      selectPattern("none");
    } else if ( src==selectIDBtn ) {
      selectPattern("id");
    } else if ( src==selectBMBtn ) {
      selectPattern("bm");
    } else if ( src==selectMACHBtn ) {
      selectPattern("elin,sy,sr");
    } else if ( src==performSearchBtn ) {
      HDBSearch();
    } else if (src ==selectNoneModBtn) {
      for(int i=0;i<22;i++) moduleCheck[i].setSelected(false);
    } else if (src ==selectAllModBtn) {
      for(int i=0;i<22;i++) moduleCheck[i].setSelected(true);
    } else if (src == timeCombo) {

      long now = System.currentTimeMillis();

      switch (timeCombo.getSelectedIndex()) {
        case 0: //Last 4 Hours
        {
          Date dstop = new java.util.Date(now);
          stopDateText.setText(dfr.format(dstop));
          Date dstart = new java.util.Date(now - 14400000);
          startDateText.setText(dfr.format(dstart));
        }
        break;
        case 1: //Last 8 Hours
        {
          Date dstop = new java.util.Date(now);
          stopDateText.setText(dfr.format(dstop));
          Date dstart = new java.util.Date(now - 28800000);
          startDateText.setText(dfr.format(dstart));
        }
        break;
        case 2: //Last 24 Hours
        {
          Date dstop = new java.util.Date(now);
          stopDateText.setText(dfr.format(dstop));
          Date dstart = new java.util.Date(now - 86400000);
          startDateText.setText(dfr.format(dstart));
        }
        break;
        case 3: //Last 3 days
        {
          Date dstop = new java.util.Date(now);
          stopDateText.setText(dfr.format(dstop));
          Date dstart = new java.util.Date(now - 259200000);
          startDateText.setText(dfr.format(dstart));
        }
        break;
        case 4: // Last week
        {
          Date dstop = new java.util.Date(now);
          stopDateText.setText(dfr.format(dstop));
          Date dstart = new java.util.Date(now - 604800000);
          startDateText.setText(dfr.format(dstart));
        }
        break;
      }

    }


  }

  public static void main(String args[]) {
    new HDBFrame().setVisible(true);
  }



}
