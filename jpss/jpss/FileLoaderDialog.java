package jpss;

import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import org.tmatesoft.svn.core.SVNException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Vector;

/**
 * File loader dialog
 */
public class FileLoaderDialog extends JDialog implements ActionListener {

  static String getFileName(JFrame parent,SVNHelper svn,File path) {

    FileLoaderDialog dlg = new FileLoaderDialog(parent,svn,path);
    return dlg.retName;

  }

  private JPanel innerPanel;

  private JScrollPane fileScroll;
  private JTable fileTable;
  private DefaultTableModel fileModel;
  private String colName[];
  private Object[][] fileInfo;

  private JPanel  buttonPanel;
  private JButton loadBtn;
  private JButton cancelBtn;

  private int selected;

  private String retName;
  private File path;

  FileLoaderDialog(JFrame parent,SVNHelper svn,File path) {

    super(parent,true);

    selected = -1;
    retName = null;
    this.path = path;

    innerPanel = new JPanel();
    innerPanel.setLayout(new BorderLayout());

    setContentPane(innerPanel);

    buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

    loadBtn = new JButton("Load selected file");
    loadBtn.addActionListener(this);
    buttonPanel.add(loadBtn);

    cancelBtn = new JButton("Cancel");
    cancelBtn.addActionListener(this);
    buttonPanel.add(cancelBtn);

    innerPanel.add(buttonPanel,BorderLayout.SOUTH);

    // -- Revision history table -------------------------------

    fileModel = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        if( columnIndex==1 ) {
          return Boolean.class;
        } else {
          return String.class;
        }
      }

      public boolean isCellEditable(int row, int column) {
          return (column==1);
      }

      public void setValueAt(Object aValue, int row, int column) {

        for(int i=0;i<getRowCount();i++) {
          super.setValueAt( i==row , i , column );
        }
        fireTableDataChanged();
        selected = row;

      }

    };

    colName = new String[2];
    colName[0] = "File";
    colName[1] = "Selection";

    fileTable = new JTable(fileModel);
    fileScroll = new JScrollPane(fileTable);
    innerPanel.add(fileScroll, BorderLayout.CENTER);

    // Build file list
    try {

      Vector<SVNStatusInfo> statusAll = svn.statusDir(path);
      Vector<SVNStatusInfo> statusFile = new Vector<SVNStatusInfo>();
      // Exclude sub directory
      for(int i=0;i<statusAll.size();i++) {
        SVNStatusInfo item = statusAll.get(i);
        File f = new File(path+"/"+item.fileName);
        if(!f.isDirectory() && f.exists()) statusFile.add(item);
      }

      fileInfo = new Object[statusFile.size()][2];

      int nbFile = 0;
      for(int i=0;i<statusFile.size();i++) {
        SVNStatusInfo item = statusFile.get(i);
        fileInfo[nbFile][0] = item.fileName;
        fileInfo[nbFile][1] = false;
        nbFile++;
      }

      fileModel.setDataVector(fileInfo,colName);
      fileTable.getColumnModel().getColumn(0).setPreferredWidth(250);

    } catch (SVNException e) {
      JOptionPane.showMessageDialog(this,e.getMessage(),"SVN Error",JOptionPane.ERROR_MESSAGE);
      return;
    }

    innerPanel.setPreferredSize(new Dimension(400,500));
    ATKGraphicsUtils.centerDialog(this);
    setTitle("Load file");
    setVisible(true);

  }

  public void actionPerformed(ActionEvent evt) {

    Object src=evt.getSource();
    if( src==cancelBtn ) {
      retName = null;
      setVisible(false);
    } else if ( src==loadBtn ) {
      if( selected==-1 ) {
        JOptionPane.showMessageDialog(this,"No file selected","Error",JOptionPane.ERROR_MESSAGE);
        return;
      }
      retName = (String)fileModel.getValueAt(selected,0);
      setVisible(false);
    }

  }

}
