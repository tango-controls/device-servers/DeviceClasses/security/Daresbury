package jpss;

import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.SVNRevision;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Vector;

/**
 * SVN status dialog
 */
public class StatusDialog extends JFrame implements ActionListener {

  private JPanel innerPanel;

  private JScrollPane statusScroll;
  private JTable statusTable;
  private DefaultTableModel statusModel;
  private String colName[];
  private Object[][] statusInfo;

  private JPanel  buttonPanel;
  private JButton dismissBtn;
  private JButton resetBtn;
  private JButton addBtn;
  private JButton deleteBtn;
  private JButton updateBtn;

  private Vector<SVNStatusInfo> status;

  private int selected = -1;

  private String repDir;
  private SVNHelper svnHelper;
  private String projectPath;

  StatusDialog(SVNHelper svnHelper,String projectPath) {

    this.svnHelper = svnHelper;
    this.projectPath = projectPath;
    innerPanel = new JPanel();
    innerPanel.setLayout(new BorderLayout());

    setContentPane(innerPanel);

    buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

    deleteBtn = new JButton("Delete");
    deleteBtn.addActionListener(this);
    buttonPanel.add(deleteBtn);

    addBtn = new JButton("Add");
    addBtn.addActionListener(this);
    buttonPanel.add(addBtn);

    updateBtn = new JButton("Update");
    updateBtn.addActionListener(this);
    buttonPanel.add(updateBtn);

    resetBtn = new JButton("Reset");
    resetBtn.addActionListener(this);
    buttonPanel.add(resetBtn);

    dismissBtn = new JButton("Dismiss");
    dismissBtn.addActionListener(this);
    buttonPanel.add(dismissBtn);

    innerPanel.add(buttonPanel,BorderLayout.SOUTH);

    // -- Status table -------------------------------

    statusModel = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        if( columnIndex==4 ) {
          return Boolean.class;
        } else {
          return String.class;
        }
      }

      public boolean isCellEditable(int row, int column) {
          return (column==4);
      }

      public void setValueAt(Object aValue, int row, int column) {

        for(int i=0;i<getRowCount();i++) {
          super.setValueAt( i==row , i , column );
        }
        fireTableDataChanged();
        selected = row;

      }

    };

    colName = new String[5];
    colName[0] = "File";
    colName[1] = "Current Revision";
    colName[2] = "Last Revision";
    colName[3] = "Status";
    colName[4] = "Selection";

    statusTable = new JTable(statusModel);
    statusScroll = new JScrollPane(statusTable);
    innerPanel.add(statusScroll,BorderLayout.NORTH);
    statusInfo = new Object[0][5];
    statusModel.setDataVector(statusInfo,colName);

    innerPanel.setPreferredSize(new Dimension(640,480));
    setTitle("Manage repository");
    ATKGraphicsUtils.centerFrameOnScreen(this);

  }

  public void actionPerformed(ActionEvent evt) {

    Object src = evt.getSource();

    if( src==dismissBtn ) {

      super.setVisible(false);

    } else if ( src==resetBtn ) {

      int ans = JOptionPane.showConfirmDialog(this,"This will retrieve the content of the server and reset your local modifications.\nDo you want to continue ?","SVN",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
      if( ans==JOptionPane.YES_OPTION ) {

        // Delete local directory
        File rep = new File(repDir);
        deleteDir(rep);
        try {
          // Now checkout a fresh release
          svnHelper.checkout(projectPath,SVNRevision.HEAD,rep);
          // Update status table
          update(repDir);
        } catch(SVNException e) {
          JOptionPane.showMessageDialog(this,e.getMessage(),"SVN Error",JOptionPane.ERROR_MESSAGE);
        }

      }

    } else if ( src==addBtn ) {

      if( selected==-1 ) {
        JOptionPane.showMessageDialog(this,"No file selected","SVN Error",JOptionPane.ERROR_MESSAGE);
        return;
      }

      String fileName = status.get(selected).fileName;

      int ans = JOptionPane.showConfirmDialog(this,"Do you want to add " + fileName + " to the repository ?","SVN",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
      if( ans==JOptionPane.YES_OPTION ) {

        try {
          File newFile = new File(repDir + "/" + fileName);
          svnHelper.add(newFile);
          svnHelper.commit(newFile,"Initial import");
          // Update status table
          update(repDir);
        } catch(SVNException e) {
          JOptionPane.showMessageDialog(this,e.getMessage(),"SVN Error",JOptionPane.ERROR_MESSAGE);
        }

      }

    } else if (src == deleteBtn) {

      if (selected == -1) {
        JOptionPane.showMessageDialog(this, "No file selected", "SVN Error", JOptionPane.ERROR_MESSAGE);
        return;
      }

      String fileName = status.get(selected).fileName;

      int ans = JOptionPane.showConfirmDialog(this, "Do you want to delete " + fileName + " from the repository ?", "SVN", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
      if (ans == JOptionPane.YES_OPTION) {

        try {
          File newFile = new File(repDir + "/" + fileName);
          svnHelper.delete(newFile);
          svnHelper.commit(newFile, fileName + " deleted");
          // Update status table
          update(repDir);
        } catch (SVNException e) {
          JOptionPane.showMessageDialog(this, e.getMessage(), "SVN Error", JOptionPane.ERROR_MESSAGE);
        }

      }

    } else if (src == updateBtn) {

      try {

        File dirFile = new File(repDir);
        svnHelper.update(dirFile,SVNRevision.HEAD);
        // Update status table
        update(repDir);

      } catch (SVNException e) {
        JOptionPane.showMessageDialog(this, e.getMessage(), "SVN Error", JOptionPane.ERROR_MESSAGE);
      }


    }

  }

  public void update(String dir) throws SVNException {

    repDir = dir;
    File homeDir = new File(dir);
    status = svnHelper.statusDir(homeDir);
    statusInfo = new Object[status.size()][5];
    for(int i=0;i<status.size();i++) {
      statusInfo[i][0] = status.get(i).fileName;
      if( status.get(i).activeRev>status.get(i).lastChangedRev) {
        statusInfo[i][1] = status.get(i).lastChangedRev;
      } else {
        statusInfo[i][1] = status.get(i).activeRev;
      }
      statusInfo[i][2] = status.get(i).lastChangedRev;
      statusInfo[i][3] = SVNHelper.StatusToStr(status.get(i).status);
      statusInfo[i][4] = false;
    }

    statusModel.setDataVector(statusInfo,colName);

  }

  public static void deleteDir(File file)  {

    if (file.isDirectory()) {

      //directory is empty, then delete it
      if (file.list().length == 0) {

        file.delete();

      } else {

        //list all the directory contents
        String files[] = file.list();

        for (String temp : files) {
          //construct the file structure
          File fileDelete = new File(file, temp);

          //recursive delete
          deleteDir(fileDelete);
        }

        //check the directory again, if empty then delete it
        if (file.list().length == 0) {
          file.delete();
        }

      }

    } else {

      file.delete();

    }

  }

}
