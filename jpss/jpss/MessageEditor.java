package jpss;

import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A class to edit a message
 */
public class MessageEditor extends JDialog implements ActionListener {

  static String editMessage(JFrame parent,String title,String defaultMessage) {

    MessageEditor dlg = new MessageEditor(parent,title,defaultMessage);
    return dlg.retString;

  }

  JPanel innerPanel;

  JPanel  buttonPanel;
  JButton okButton;
  JButton cancelButton;

  JTextArea theText;
  JScrollPane theScroll;

  String retString;

  MessageEditor(JFrame parent,String title,String defaultMessage) {

    super(parent,true);

    retString = null;

    innerPanel = new JPanel();
    innerPanel.setLayout(new BorderLayout());

    buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    innerPanel.add( buttonPanel , BorderLayout.SOUTH );

    okButton = new JButton("OK");
    okButton.addActionListener(this);
    buttonPanel.add(okButton);

    cancelButton = new JButton("Cancel");
    cancelButton.addActionListener(this);
    buttonPanel.add(cancelButton);

    theText = new JTextArea();
    theText.setText(defaultMessage);
    theText.setEditable(true);
    theScroll = new JScrollPane(theText);
    innerPanel.add(theScroll,BorderLayout.CENTER);

    innerPanel.setPreferredSize(new Dimension(400,300));
    setTitle(title);
    setContentPane(innerPanel);
    ATKGraphicsUtils.centerDialog(this);
    setVisible(true);

  }

  public void actionPerformed(ActionEvent evt) {

    Object src=evt.getSource();
    if( src==cancelButton ) {
      retString = null;
      setVisible(false);
    } else if ( src==okButton ) {
      retString = theText.getText();
      setVisible(false);
    }

  }


}
