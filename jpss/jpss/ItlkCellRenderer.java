package jpss;

/**
 * PSS Interlock cell renderer
 */

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class ItlkCellRenderer extends JPanel implements TableCellRenderer {

  final static Color selColor = new Color(184,207,229);
  Color bubbleColors[] = new Color[4];
  boolean validValue;

  public ItlkCellRenderer() {
    setOpaque(true);
    setPreferredSize(new Dimension(100,25));
  }

  public Component getTableCellRendererComponent(JTable table,Object value,
                                                 boolean isSelected, boolean hasFocus, int row, int column) {

    Object val = table.getModel().getValueAt(row, column);

    validValue = (val instanceof Long);

    if (isSelected) {
      setBackground(selColor);
    } else {
      setBackground(Color.WHITE);
    }

    if( validValue ) {
      int itlkValue = ((Long)val).intValue();
      validValue = itlkValue>=0 && itlkValue<=255;
      if( validValue ) {
        computeState(itlkValue, 0);
        computeState(itlkValue, 1);
        computeState(itlkValue, 2);
        computeState(itlkValue, 3);
      }
    }
    return this;

  }

  private void computeState(int val,int idx) {

    int b = (val >> (2*idx)) & 3;
    switch(b) {
      case 0:
        bubbleColors[idx] = Color.RED;
        break;
      case 1:
      case 2:
        bubbleColors[idx] = Color.ORANGE;
        break;
      case 3:
        bubbleColors[idx] = Color.GREEN;
        break;
    }

  }

  private void paintBubble(Graphics g,int idx) {
    g.setColor(bubbleColors[idx]);
    g.fillOval(idx*12+1,2,10,10);
    g.setColor(Color.BLACK);
    g.drawOval(idx*12+1,2,10,10);
  }

  public void paint(Graphics g) {
    super.paint(g);
    if(validValue) {
      paintBubble(g, 0);
      paintBubble(g, 1);
      paintBubble(g, 2);
      paintBubble(g, 3);
    }
  }

}

