package jpss;

import fr.esrf.tangoatk.core.ATKException;
import fr.esrf.tangoatk.core.Device;
import fr.esrf.tangoatk.core.DeviceFactory;
import fr.esrf.tangoatk.widget.util.interlock.*;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.SVNRevision;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.*;
import java.awt.*;
import java.util.Vector;


/**
 * A high level class to display a PSS interlock scheme (evaluate logical diagram).
 */
public class PssNetViewerFrame extends JFrame implements ActionListener,NetEditorListener,Reloadable,ChangeListener {

  private String  appTitle;
  private boolean runningFromShell =true;

  // components
  JMenuBar mainMenu;

  JMenu jMenuFile;
  JMenuItem jMenuItemLoad;
  JMenuItem jMenuItemExit;

  JMenu jMenuHistory;
  JMenuItem jMenuItemNoHistory;
  JMenuItem[] jMenuMemHist;
  JMenuItem[] jMenuHDBHist;

  JMenu       jMenuCommand;
  JMenuItem   jMenuItemReset;
  JMenuItem   jMenuItemError;
  JMenuItem[] revManageMenuItem = new JMenuItem[PssConst.MAX_CRATE];

  PssNetViewer[] iV = new PssNetViewer[PssConst.MAX_CRATE];
  JScrollPane[] js = new JScrollPane[PssConst.MAX_CRATE];
  boolean[] hasCrate = new boolean[PssConst.MAX_CRATE];
  int maxCrate;
  JTabbedPane tabPane;

  // SVN stuff
  private   String    tmpDir;
  private   File      homeDir;
  private   SVNHelper svnHelper;
  private   String    projectPath;
  private   File      projectDir;
  private   RevisionDialog revDlg = null;

  // Current loaded files
  private String[] currentFile = new String[PssConst.MAX_CRATE];

  public PssNetViewerFrame(String fileName) {
    this(fileName,false);
    ATKGraphicsUtils.centerFrameOnScreen(this);
    setVisible(true);
  }

  public PssNetViewerFrame(String fileName,boolean runningFromShell) {
    this(fileName, runningFromShell,false);
  }

  public PssNetViewerFrame(String fileName,boolean runningFromShell,boolean hasReset) {

    this.runningFromShell = runningFromShell;
    for(int i=0;i<PssConst.MAX_CRATE;i++)
      hasCrate[i] = false;
    tmpDir=System.getProperty("user.home")+"/.jpss";

    // Special handling for beamline

    if( fileName.length()==0 ) {

      // Check BEAMLINENAME
      String blName = System.getenv("BEAMLINENAME");
      if( blName==null || blName.length()==0 ) {
        JOptionPane.showMessageDialog(this,"You have either to specify a pss file \n"+
                                           "or to define BEAMLINENAME environement variable","Error",JOptionPane.ERROR_MESSAGE);
        throw new IllegalStateException("Wrong input parameters");
      }

      blName = blName.toUpperCase();
      int blNumber = 0;
      String crName;

      if(blName.startsWith("D")) {
        blNumber = extractNumber(blName.substring(1));
        crName = "bm" + String.format("%02d",blNumber);
      } else if(blName.startsWith("ID")) {
        blNumber = extractNumber(blName.substring(2));
        crName = "id" + String.format("%02d",blNumber);
      } else {
        JOptionPane.showMessageDialog(this,"Wrong BEAMLINENAME definition IDxx or Dxx","Error",JOptionPane.ERROR_MESSAGE);
        throw new IllegalStateException("Wrong input parameters");
      }

      projectPath = "beamlines/pss";

      for(int i=0;i<PssConst.MAX_CRATE;i++) {
        hasCrate[i] = hasCrate(crName,i);
        if(hasCrate[i]) {
          currentFile[i] = tmpDir + "/" + projectPath + "/" + crName.toLowerCase();
          if(i!=0)
            currentFile[i] += "." + Integer.toString(i+1) + ".net";
          else
            currentFile[i] += ".net";
          maxCrate = i+1;
        }
      }

    } else {

      hasCrate[0] = true;
      projectPath = fileName.substring(0,fileName.lastIndexOf('/'));
      currentFile[0] = tmpDir + "/" + fileName;
      maxCrate = 1;

    }


    // Create SVN local copy of the PSS project ------------------------------------------------

    homeDir = new File(tmpDir);
    if( !homeDir.exists() ) {
      System.out.println("Creating .jpss directory...");
      homeDir.mkdir();
    }
    projectDir = new File(tmpDir+"/"+projectPath);
    File pssFile = new File(tmpDir + "/" + fileName);

    try {

      svnHelper = new SVNHelper(PssConst.SVN_SERVER);

      File svnDir = new File(tmpDir+"/"+".svn");

      if (svnDir.exists()) {

        for (int i = 0; i < PssConst.MAX_CRATE; i++) {
          if (hasCrate[i]) {

            // Check if a new release of the PSS file is available
            int status = svnHelper.status(new File(currentFile[i]));
            if (status == SVNHelper.SVN_STATUS_NEEDS_UPDATE) {

              int ans = JOptionPane.showConfirmDialog(this, "A more recent release of " + fileName + " exists on the master server.\nDo you want to update ?", "SVN", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
              if (ans == JOptionPane.YES_OPTION) {
                // Do update
                svnHelper.update(pssFile, SVNRevision.HEAD);
              }

            }

          }
        }

      } else {

        // Check out the SVN repo
        svnHelper.checkout(projectPath,SVNRevision.HEAD,projectDir);

      }

    } catch( SVNException e) {

      JOptionPane.showMessageDialog(null,e.getMessage(),"SVN Error",JOptionPane.ERROR_MESSAGE);

    }

    // Menu ----------------------------------------------------------------------------

    mainMenu = new JMenuBar();

    jMenuFile = new JMenu();
    jMenuFile.setText("File");
    jMenuFile.setMnemonic('F');
    jMenuItemLoad   = createMenuItem("Load file...",0,0);
    jMenuItemExit   = createMenuItem("Exit",0,0);
    jMenuFile.add(jMenuItemLoad);
    jMenuFile.add(jMenuItemExit);

    jMenuCommand = new JMenu();
    jMenuCommand.setText("Command");
    jMenuCommand.setMnemonic('C');
    if(hasReset) {
      jMenuItemReset   = createMenuItem("Reset PSS",0,0);
      jMenuCommand.add(jMenuItemReset);
    }
    jMenuItemError   = createMenuItem("View errors",0,0);
    jMenuCommand.add(jMenuItemError);

    jMenuHistory = new JMenu();
    jMenuHistory.setText("History");
    jMenuHistory.setMnemonic('H');
    jMenuItemNoHistory = createMenuItem("Disable history viewing",0,0);

    JMenu revMenu = new JMenu("Revision");
    for(int i=0;i<maxCrate;i++) {
      revManageMenuItem[i] = new JMenuItem("Manage [no file]");
      revManageMenuItem[i].addActionListener(this);
      revManageMenuItem[i].setVisible(hasCrate[i]);
      revMenu.add(revManageMenuItem[i]);
    }

    mainMenu.add(jMenuFile);
    mainMenu.add(jMenuCommand);
    mainMenu.add(jMenuHistory);
    mainMenu.add(revMenu);

    setJMenuBar(mainMenu);

    // Itlk Viewer ----------------------------------------------------------------------

    tabPane = new JTabbedPane();
    tabPane.addChangeListener(this);

    for(int i=0;i<maxCrate;i++) {
      iV[i] = new PssNetViewer(this);
      iV[i].addEditorListener(this);
      js[i] = new JScrollPane(iV[i]);
      tabPane.addTab("Crate " + Integer.toString(i+1),js[i]);
      reload(i);
    }

    setContentPane(tabPane);

    pack();
    addWindowListener(new WindowListener() {
      public void windowOpened(WindowEvent e) {}
      public void windowClosing(WindowEvent e) {
        exitForm();
      };
      public void windowClosed(WindowEvent e) {}
      public void windowIconified(WindowEvent e) {}
      public void windowDeiconified(WindowEvent e) {}
      public void windowActivated(WindowEvent e) {}
      public void windowDeactivated(WindowEvent e) {}
    }
    );


  }

  // Exit application
  public void exitForm() {

    if(runningFromShell) {
      System.exit(0);
    } else {
      for(int i=0;i<maxCrate;i++)
        iV[i].clearModel();
      setVisible(false);
      dispose();
    }

  }

  private boolean hasCrate(String name,int crate) {

    String crName = "exp/" + name + "-cr" + Integer.toString(crate+1) + "/sl0";
    Device d = null;
    try {
      d = DeviceFactory.getInstance().getDevice(crName);
    } catch (ATKException e) {}
    return d!=null;

  }

  // -----------------------------------------------------
  // Action Listener
  // -----------------------------------------------------
  public void actionPerformed(java.awt.event.ActionEvent evt) {

    Object src = evt.getSource();
    int selectedTab = tabPane.getSelectedIndex();

    // Static source

    if (src == jMenuItemLoad) {

      String name = FileLoaderDialog.getFileName(this,svnHelper,projectDir);
      if(name!=null) {
        currentFile[selectedTab] = projectDir.getAbsolutePath()+"/"+name;
        reload();
      }

    } else if (src == jMenuItemExit) {

      exitForm();

    } else if (src == jMenuItemNoHistory) {

      iV[selectedTab].restoreNormalMode();
      String fName = new File(currentFile[selectedTab]).getName();
      tabPane.setTitleAt(selectedTab,fName);

    } else if (src == jMenuItemReset) {

      iV[selectedTab].resetPSS();

    } else if (src == jMenuItemError) {

      iV[selectedTab].showErrors();

    } else {

      // Search Revision menu
      int i = 0;
      boolean found = false;
      while (i < revManageMenuItem.length && !found) {
        found = (revManageMenuItem[i] == src);
        if (!found) i++;
      }
      if (found) {
        showRevision(i);
        return;
      }

      // Search in history (mem) menu item
      i = 0;
      found = false;
      while (i < jMenuMemHist.length && !found) {
        found = (jMenuMemHist[i] == src);
        if (!found) i++;
      }
      if (found) {
        iV[selectedTab].showMemHistory(i);
        String fName = new File(currentFile[selectedTab]).getName();
        tabPane.setTitleAt(selectedTab,iV[selectedTab].computeTabTitle(fName));
      }

      if(!found) {
        // Search in history (hdb) menu item
        i = 0;
        found = false;
        while (i < jMenuHDBHist.length && !found) {
          found = (jMenuHDBHist[i] == src);
          if (!found) i++;
        }
        if (found) {
          iV[selectedTab].showHDBHistory(i);
          String fName = new File(currentFile[selectedTab]).getName();
          tabPane.setTitleAt(selectedTab, iV[selectedTab].computeTabTitle(fName));
        }
      }

    }

  }

  private void showRevision(int tab) {

    if( revDlg==null ) revDlg = new RevisionDialog(false,svnHelper,this);
    if(currentFile!=null && currentFile[tab].length()>0) {
      try {
        revDlg.setFilename(currentFile[tab]);
        revDlg.setVisible(true);
      } catch(SVNException e) {
        JOptionPane.showMessageDialog(this,e.getMessage(),"SVN Error",JOptionPane.ERROR_MESSAGE);
      }
    } else {
      JOptionPane.showMessageDialog(this,"No file loaded","Error",JOptionPane.ERROR_MESSAGE);
    }

  }

  private void buildHisotryMenu(int s) {

    int i;
    Vector models = iV[s].getModels();
    jMenuHistory.removeAll();

    // Build the history menu
    jMenuHistory.add(jMenuItemNoHistory);
    jMenuHistory.add(new JSeparator());
    jMenuMemHist = new JMenuItem[models.size()];
    jMenuHDBHist = new JMenuItem[models.size()];
    for (i = 0; i < jMenuMemHist.length; i++) {
      Device ds = (Device)(((ItlkModel) models.get(i)).model.getDevice());
      String devName = ds.name();
      JMenu devMem = new JMenu(devName);
      jMenuHistory.add(devMem);

      jMenuMemHist[i] = createMenuItem("From memory", 0, 0);
      devMem.add(jMenuMemHist[i]);
      jMenuHDBHist[i] =  createMenuItem("From HDB", 0, 0);
      devMem.add(jMenuHDBHist[i]);
    }

  }

  @Override
  public void stateChanged(ChangeEvent e) {
    buildHisotryMenu(tabPane.getSelectedIndex());
  }

  // ----------------------------------------------------------
  // NetEditor listener
  // ----------------------------------------------------------

  public void valueChanged(NetEditor src) {

     appTitle = "JPSS " + PssConst.APP_RELEASE + " [" + projectPath + "]";
     setTitle(appTitle);

  }

  public void sizeChanged(NetEditor src,Dimension d) {
    for(int i=0;i<maxCrate;i++)
      if(hasCrate[i] && js[i]!=null)
        js[i].revalidate();
    repaint();
  }

  public void cancelCreate(NetEditor src) {}

  public void linkClicked(NetEditor src, NetObject obj, int childIdx, java.awt.event.MouseEvent e) {}

  public void objectClicked(NetEditor src, NetObject obj, java.awt.event.MouseEvent e) {}

  // ----------------------------------------------------------
  // Private stuff
  // ----------------------------------------------------------

  // Create item menu
  private JMenuItem createMenuItem(String name, int key, int modifier) {
    JMenuItem m = new JMenuItem();
    m.setText(name);
    if (key != 0)
      m.setAccelerator(KeyStroke.getKeyStroke(key, modifier));
    m.addActionListener(this);
    return m;
  }

  @Override
  public void reload() {
    int selectedTab = tabPane.getSelectedIndex();
    reload(selectedTab);
  }

  public void reload(int selectedTab) {
    if (currentFile[selectedTab] != null) {
      try {
        String fName = new File(currentFile[selectedTab]).getName();
        iV[selectedTab].setModel(currentFile[selectedTab]);
        revManageMenuItem[selectedTab].setText("Manage [" + fName + "]");
        buildHisotryMenu(selectedTab);
        tabPane.setTitleAt(selectedTab, fName);
      } catch (IOException ex) {
        JOptionPane.showMessageDialog(null, "Error during reading file:" + currentFile[selectedTab] + "\n" + ex.getMessage());
      }
    }
  }

  private int extractNumber(String s) {

    int i=0;
    StringBuffer str = new StringBuffer();
    while(i<s.length() && s.charAt(i)>='0' && s.charAt(i)<='9') {
      str.append(s.charAt(i));
      i++;
    }
    int n = Integer.parseInt(str.toString());
    System.out.println("BL number="+n);
    return n;

  }

  // ----------------------------------------------------------
  // Main function
  // ----------------------------------------------------------

  public static void main(String[] args) {

    final PssNetViewerFrame jpss;
    if (args.length != 1) {
      String blName = System.getenv("BEAMLINENAME");
      if( blName==null || blName.length()==0 ) {
        System.out.println("Usage: jpssview [filename]");
        System.out.println("ex:    jpssview machine/pss/detailed/Crate1.net");
        System.out.println("       jpssview machine/pss/sr1.net");
        System.out.println("       jpssview beamlines/pss/bm01.net");
        System.exit(0);
      }
      jpss = new PssNetViewerFrame("",true);
    } else {
      jpss = new PssNetViewerFrame(args[0],true);
    }
    ATKGraphicsUtils.centerFrameOnScreen(jpss);
    jpss.setVisible(true);

  }

}
