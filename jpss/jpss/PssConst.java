package jpss;

/**
 * Created by pons on 19/02/16.
 */
public class PssConst {

  public final static String APP_RELEASE = "3.5";
  public final static int MAX_CRATE = 3;
  public final static String SVN_SERVER = "svn://acs.esrf.fr";
  public final static String TANGO_HOST = "tango://acs.esrf.fr:10000/";

}
