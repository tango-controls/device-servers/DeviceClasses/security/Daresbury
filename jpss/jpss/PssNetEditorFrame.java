package jpss;

import fr.esrf.tangoatk.widget.util.interlock.NetEditorFrame;
import fr.esrf.tangoatk.widget.util.interlock.NetUtils;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import javax.swing.*;
import fr.esrf.tangoatk.widget.util.interlock.*;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.SVNRevision;

import java.awt.event.*;
import java.io.File;

/** The main Interlock editor frame */
public class PssNetEditorFrame extends NetEditorFrame implements Reloadable {

  // The Interlock editor component
  PssNetEditor  itlkEditor;

  // New menu items
  JMenuItem traceMode;
  JMenuItem editMode;
  JMenu     globalEdit;
  JMenuItem deviceEdit;
  JMenuItem fileLoadMenu;
  JMenuItem revManageMenuItem;
  JMenuItem revStatusMenuItem;

  // New toolbar items
  JButton bubbleBtn;
  JButton permitBtn;
  JButton joinBtn;
  JButton vccBtn;
  JButton groundBtn;

  // SVN stuff
  private   String    tmpDir;
  private   File      homeDir;
  private   SVNHelper svnHelper;
  private   String    projectPath;
  private   File      projectDir;
  private   RevisionDialog revDlg = null;
  private   StatusDialog statusDlg = null;

  // Current loaded file
  private String currentFile = null;


  // ---------------------------------------------------------------
  // Construction
  // ---------------------------------------------------------------
  public PssNetEditorFrame(String path) {

    setAppTitle("JPSS editor " + PssConst.APP_RELEASE + " (" + path + ")");

    // Create SVN local copy of the PSS project ------------------------------------------------

    projectPath = path;
    tmpDir=System.getProperty("user.home")+"/.jpss";
    homeDir = new File(tmpDir);
    if( !homeDir.exists() ) {
      System.out.println("Creating .jpss directory...");
      homeDir.mkdir();
    }
    projectDir = new File(tmpDir+"/"+projectPath);
    setCurrentDirectory(projectDir.getAbsolutePath());

    try {

      svnHelper = new SVNHelper(PssConst.SVN_SERVER);

      File svnDir = new File(tmpDir+"/"+".svn");

      if( !svnDir.exists() ) {
        // checkout
        svnHelper.checkout(projectPath,SVNRevision.HEAD,projectDir);

      }

      //svnHelper.status(homeDir);

    } catch( SVNException e) {

      JOptionPane.showMessageDialog(null,e.getMessage(),"SVN Error",JOptionPane.ERROR_MESSAGE);

    }

    // Create the editor -------------------------------------------
    itlkEditor = new PssNetEditor(this);
    setEditor(itlkEditor);

    // Customise file menu
    fileLoadMenu = getFileMenuItem(FILE_LOAD);

    // Customize edit menu
    globalEdit = new JMenu("Global");
    deviceEdit = NetUtils.createMenuItem("Device",0,0,this);
    globalEdit.add(deviceEdit);
    getEditMenu().add( new JSeparator() );
    getEditMenu().add( globalEdit );

    // Customize options menu ---------------------------------------
    traceMode = NetUtils.createMenuItem("Trace mode",0,0,this);
    editMode = NetUtils.createMenuItem("Edit mode",0,0,this);
    getOptionMenu().add(traceMode,0);
    getOptionMenu().add(editMode,1);
    getOptionMenu().add(new JSeparator(),2);

    // Add Revision menu --------------------------------------------
    JMenu revMenu = new JMenu("Revision");
    revManageMenuItem = new JMenuItem("Manage [no file]");
    revManageMenuItem.addActionListener(this);
    revMenu.add(revManageMenuItem);

    revStatusMenuItem = new JMenuItem("Global status");
    revStatusMenuItem.addActionListener(this);
    revMenu.add(revStatusMenuItem);

    getJMenuBar().add(revMenu);

    // Cutomize toollbar --------------------------------------------
    JToolBar tb = getToolbar();
    tb.remove(getToobarButton(NetEditorFrame.TOOL_BUBBLE));
    String rPth = "/fr/esrf/tangoatk/widget/util/interlock/gif/";
    bubbleBtn = createIconButton(rPth,"bubble","Create an interlock object (physical switch)",this);
    tb.add(bubbleBtn,0);
    permitBtn = createIconButton(rPth,"permit","Create a permit object (logical sensor)",this);
    tb.add(permitBtn,1);
    joinBtn = createIconButton(rPth,"join","Create a join object (intersection point)",this);
    tb.add(joinBtn,2);
    vccBtn = createIconButton(rPth,"start","Create a VCC object",this);
    tb.add(vccBtn,3);
    groundBtn = createIconButton(rPth,"end","Create a Ground object",this);
    tb.add(groundBtn,4);

  }

  public static boolean setCurrentDirectory(String directory_name)
  {
    boolean result = false;  // Boolean indicating whether directory was set
    File    directory;       // Desired current working directory

    directory = new File(directory_name).getAbsoluteFile();
    if (directory.exists() || directory.mkdirs())
    {
      result = (System.setProperty("user.dir", directory.getAbsolutePath()) != null);
    }

    return result;
  }

  public void objectClicked(NetEditor src,NetObject obj,MouseEvent e) {

    itlkEditor.swapItlkState(obj);

  }

  public void actionPerformed(ActionEvent e) {

    Object src = e.getSource();
    if( src == bubbleBtn ) {
      itlkEditor.setCreateMode(NetEditor.CREATE_BUBBLE,PssNetEditor.ITLK_BUBBLE);
      getHelpLabel().setText("Interlock creation: Left click to create a interlock object, right click to cancel.");
    } else if (src == permitBtn ) {
      itlkEditor.setCreateMode(NetEditor.CREATE_BUBBLE,PssNetEditor.SENSOR_BUBBLE);
      getHelpLabel().setText("Sensor creation: Left click to create a sensor object, right click to cancel.");
    } else if (src == joinBtn ) {
      itlkEditor.setCreateMode(NetEditor.CREATE_BUBBLE,PssNetEditor.JOIN_BUBBLE);
      getHelpLabel().setText("Join creation: Left click to create a join object, right click to cancel.");
    } else if (src == vccBtn ) {
      itlkEditor.setCreateMode(NetEditor.CREATE_BUBBLE,PssNetEditor.VCC_BUBBLE);
      getHelpLabel().setText("VCC creation: Left click to create a VCC object, right click to cancel.");
    } else if (src == groundBtn ) {
      itlkEditor.setCreateMode(NetEditor.CREATE_BUBBLE,PssNetEditor.GROUND_BUBBLE);
      getHelpLabel().setText("Ground creation: Left click to create a ground object, right click to cancel.");
    } else if (src == traceMode) {
      itlkEditor.setEditable(false);
    } else if (src == editMode) {
      itlkEditor.setEditable(true);
    } else if (src == deviceEdit ) {
      String nDevice = JOptionPane.showInputDialog(this,"Apply this device name to the selection","Global Edit",JOptionPane.INFORMATION_MESSAGE);
      if( nDevice!=null ) {
        itlkEditor.setDeviceName(nDevice);
      }
    } else if ( src==fileLoadMenu ) {
      String name = FileLoaderDialog.getFileName(this,svnHelper,projectDir);
      if(name!=null) {
        currentFile = projectDir.getAbsolutePath()+"/"+name;
        reload();
      }
    } else if (src == revManageMenuItem) {
      showRevision();
    } else if (src == revStatusMenuItem) {
      showStatus();
    }  else {
      super.actionPerformed(e);
    }

  }

  private void showRevision() {

    if( revDlg==null ) revDlg = new RevisionDialog(true,svnHelper,this);
    if(currentFile!=null && currentFile.length()>0) {
      try {
        revDlg.setFilename(currentFile);
        revDlg.setVisible(true);
      } catch(SVNException e) {
        JOptionPane.showMessageDialog(this,e.getMessage(),"SVN Error",JOptionPane.ERROR_MESSAGE);
      }
    } else {
      JOptionPane.showMessageDialog(this,"No file loaded","Error",JOptionPane.ERROR_MESSAGE);
    }

  }

  private void showStatus() {

    if( statusDlg==null ) statusDlg = new StatusDialog(svnHelper,projectPath);
    try {
      statusDlg.update(projectDir.getAbsolutePath());
      statusDlg.setVisible(true);
    } catch(SVNException e) {
      JOptionPane.showMessageDialog(this,e.getMessage(),"SVN Error",JOptionPane.ERROR_MESSAGE);
    }

  }

  // -----------------------------------------------------
  // Main function
  // -----------------------------------------------------
  public static void fatalError() {
    System.out.println("Usage: jpssed project_path");
    System.out.println("ex:    jpssed machine/pss/detailed");
    System.out.println("       jpssed machine/pss");
    System.out.println("       jpssed beamlines/pss");
    System.exit(0);
  }

  public static void main(String[] args) {

    if(args.length!=1) {
      fatalError();
    }
    PssNetEditorFrame iE = new PssNetEditorFrame(args[0]);
    ATKGraphicsUtils.centerFrameOnScreen(iE);
    iE.setVisible(true);

  }

  @Override
  public void reload() {
    loadFile(currentFile);
    File f = new File(currentFile);
    revManageMenuItem.setText("Manage [" + f.getName() + "]");
  }
}


