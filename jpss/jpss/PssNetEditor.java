package jpss;

import fr.esrf.tangoatk.widget.util.interlock.*;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/** A class which override the NetEditor to build an Interlock Simulator editor */
public class PssNetEditor extends NetEditor {

  // Interlock Bubble Type
  final static int ITLK_BUBBLE   = 1;  // Represents a physical interlock (as a switch)
  final static int SENSOR_BUBBLE = 2;  // Represents a logical sensor
  final static int VCC_BUBBLE    = 3;  // Represents a VCC object
  final static int GROUND_BUBBLE = 4;  // Represents a ground object
  final static int JOIN_BUBBLE   = 5;  // Represents an intersection point

  // Iterlock state
  final static int UNKNOWN_STATE = 0;
  final static int CLOSE_STATE   = 1;
  final static int ALARM_STATE   = 2;
  final static int OPEN_STATE    = 3;

  private int createMode;
  private NetObject theRoot;  // root VCC object (only one allowed)
  private BasicStroke linkBs = new BasicStroke(2);

  // Set of ITLK_BUBBLE extensions
  static String bubbleExts[] = {
    "Device",
    "Board",
    "Relay",
    "Type",
    "Net Name"
  };

  static String permitExts[] = {
    "Type",
    "Net Name"
  };

  /** Construction */
  public PssNetEditor(JFrame parent) {

    super(parent);
    createMode = 0;
    theRoot=null;

  }

  /** Override loadFile to reinitialise object struct after a load */
  public void loadFile(String fileName) throws IOException {

    super.loadFile(fileName);
    if(!isEditable()) prepareObjects();

  }

  /** Sets the editor in EDIT/TRACE mode */
  public void setEditable(boolean b) {

    if(!b) prepareObjects();
    super.setEditable(b);

  }

  /** Returns the root of the scheme (The only VCC object) or null */
  public NetObject getRoot() {
    return theRoot;
  }

  /** Prepare object for logical evaluation, must be called once before playing logic */
  public void prepareObjects() {

    for (int i = 0; i < getNetObjectNumber(); i++) {
      NetObject o = getNetObjectAt(i);
      if (o.getType() != NetObject.OBJECT_TEXT) {
        o.setUserValue(new ItlkInfo());
        getII(o).state = CLOSE_STATE; /* Initial state to CLOSE(green) */
        o.setColor(Color.GREEN);
      }
    }

  }

  /** Reset algorithm variable */
  private void resetLogic() {

    for (int i = 0; i < getNetObjectNumber(); i++) {
      NetObject o = getNetObjectAt(i);
      if (o.getType() != NetObject.OBJECT_TEXT) {
        getII(o).nbHit = o.getParentNumber();
        getII(o).storedResult = OPEN_STATE;
      }
    }

  }

  /** Compute logical or */
  private int or(int s1,int s2) {

    switch(s1) {
      case UNKNOWN_STATE:
        switch( s2 ) {
          case CLOSE_STATE:
            return CLOSE_STATE;
        }
        break;
      case CLOSE_STATE:
        return CLOSE_STATE;
      case OPEN_STATE:
        switch( s2 ) {
          case CLOSE_STATE:
            return CLOSE_STATE;
          case OPEN_STATE:
            return OPEN_STATE;
        }
        break;
    }

    return UNKNOWN_STATE;
  }

  /** Compute state of link and output (sensor), paint links */
  private void computeState(Graphics2D g,NetObject o,int curState) {

    if (getII(o).nbHit > 1) {

      // Multiple input
      // Wait for the end of the branch
      getII(o).storedResult = or(getII(o).storedResult,curState);
      getII(o).nbHit--;

    } else {

      switch (o.getUserType()) {

        case ITLK_BUBBLE:
          switch(getII(o).state) {
            case CLOSE_STATE:
              propagateState(g,o, curState);
              break;
            case OPEN_STATE:
            case ALARM_STATE:
              propagateState(g,o, OPEN_STATE);
              break;
            case UNKNOWN_STATE:
              if(curState==OPEN_STATE)
                propagateState(g,o, OPEN_STATE);
              else
                propagateState(g,o, UNKNOWN_STATE);
              break;
          }
          break;

        // Logic for an intersection
        case JOIN_BUBBLE:
          if (o.getParentNumber() > 1) {
            getII(o).storedResult = or(getII(o).storedResult,curState);
            propagateState(g,o, getII(o).storedResult);
          } else {
            propagateState(g,o, curState);
          }
          break;

        // Logic for an startpoint
        case VCC_BUBBLE:
          propagateState(g,o, curState);
          break;

        // Logic for a permit
        case SENSOR_BUBBLE:
          setState(o,curState);
          propagateState(g,o, curState);
          break;
      }

    }

  }

  /** propagate state on children */
  private void propagateState(Graphics2D g, NetObject o,int curState) {

    for (int i = 0; i < o.getChildrenNumber(); i++) {
      NetObject c = o.getChildAt(i);
      switch (curState) {
        case CLOSE_STATE:
          g.setColor(Color.GREEN);
          break;
        case ALARM_STATE:
        case OPEN_STATE:
          g.setColor(Color.RED);
          break;
        default:
          g.setColor(Color.GRAY);
      }
     Stroke old = g.getStroke();
     g.setStroke(linkBs);
     o.paintLink(g, c, false);
     g.setStroke(old);
     computeState(g, c, curState);
    }

  }

  /** Swap an iterlock state */
  public void swapItlkState(NetObject o) {

    if( getII(o).state == CLOSE_STATE) {
      getII(o).state = OPEN_STATE;
      o.setColor(Color.RED);
    } else {
      getII(o).state = CLOSE_STATE;
      o.setColor(Color.GREEN);
    }
    repaint();

  }

  public boolean checkRelayConfig(NetObject o) {

    String dev = o.getExtendedParam("Device");
    if( dev.length()==0 ) {
      getII(o).configError = "No device name defined for this bubble.";
      return false;
    }

    getII(o).devName = dev;

    try {
      // !!! PSS address start from 1 !!!
      getII(o).wordAddress = Integer.parseInt(o.getExtendedParam("Board"))-1;
      getII(o).bitNumber   = ((Integer.parseInt(o.getExtendedParam("Relay"))-1)*2) % 16; // Bit address
    } catch(NumberFormatException ex) {
      getII(o).wordAddress = -1;
      getII(o).bitNumber = -1;
      getII(o).configError = "Invalid extension value (Board or Relay) :" + "\n" + ex.getMessage();
      return false;
    }

    return true;
  }

  public void setRelayState(NetObject o,int stateA,int stateB) {
    getII(o).stateA = stateA;
    getII(o).stateB = stateB;
  }

  public int getRelayAState(NetObject o) {
    return getII(o).stateA;
  }

  public int getRelayBState(NetObject o) {
    return getII(o).stateB;
  }

  public void setState(NetObject o,int state) {

    getII(o).state = state;

    switch( state ) {
      case CLOSE_STATE:
        o.setColor(Color.GREEN);
        break;
      case ALARM_STATE:
        o.setColor(Color.ORANGE);
        break;
      case OPEN_STATE:
        o.setColor(Color.RED);
        break;
      default:
        o.setColor(Color.GRAY);
        break;
    }

  }

  /** Helper function to retreive the ItlkInfo */
  public ItlkInfo getII(NetObject o) {
    return (ItlkInfo)o.getUserValue();
  }

  /** Sets the editor in creation mode */
  public void setCreateMode(int type,int userType) {
    createMode =  userType;
    setCreateMode(type);
  }

  /** Overriding createBubbleObject to create our specific NetObject */
  public NetObject createBubbleObject(int x,int y) {

    NetObject ret = null;

    switch(createMode) {

      case ITLK_BUBBLE:
        ret = new NetObject(NetObject.OBJECT_BUBBLE,ITLK_BUBBLE,1,1,x,y);
        ret.setShape(NetShape.SHAPE_CIRCLE);
        ret.setExtensionList(bubbleExts);
        ret.setEditableShape(false);
        break;
      case SENSOR_BUBBLE:
        ret = new NetObject(NetObject.OBJECT_BUBBLE,SENSOR_BUBBLE,1,1,x,y);
        ret.setShape(NetShape.SHAPE_SQUARE);
        ret.setExtensionList(permitExts);
        ret.setEditableShape(false);
        ret.setSize(9);
        break;
      case VCC_BUBBLE:
        ret = new NetObject(NetObject.OBJECT_BUBBLE,VCC_BUBBLE,0,1,x,y);
        ret.setShape(NetShape.SHAPE_VCC);
        ret.setEditableShape(false);
        ret.setLabel("Vcc");
        break;
      case GROUND_BUBBLE:
        ret = new NetObject(NetObject.OBJECT_BUBBLE,GROUND_BUBBLE,1,0,x,y);
        ret.setShape(NetShape.SHAPE_GROUND);
        ret.setEditableShape(false);
        break;
      case JOIN_BUBBLE:
        ret = new NetObject(NetObject.OBJECT_BUBBLE,JOIN_BUBBLE,10,10,x,y);
        ret.setShape(NetShape.SHAPE_DOT);
        ret.setEditableShape(false);
        break;

    }

    return ret;

  }

  /** Override addObject to ensure that there is always at most one VCC object */
  public boolean addObject(NetObject o) {

    if (o == null)
      return false;

    if (o.getUserType() == VCC_BUBBLE) {
      if (theRoot != null) {
        JOptionPane.showMessageDialog(getParentFrame(), "Cannot add a new VCC object. Only 1 accepted",
                "Error", JOptionPane.ERROR_MESSAGE);
        return false;
      } else {
        theRoot = o;
      }
    }

    return super.addObject(o);

  }

  /** Override removeObject to ensure that there is always at most one VCC object */
  public void removeObject(NetObject o) {

    if(theRoot==o) theRoot=null;
    super.removeObject(o);

  }

  /** Override clearObjects to ensure that there is always at most one VCC object */
  public void clearObjects() {

    theRoot=null;
    super.clearObjects();

  }

  /** Override paintLink to paint link state */
  public void paintLinks(Graphics2D g2) {

    if(!isEditable()) {
      resetLogic();
      if(theRoot!=null) computeState(g2,theRoot,CLOSE_STATE);
    } else {
      super.paintLinks(g2);
    }

  }

  /** Override paintObject to map the type and address extension to small label before objects are painted */
  public void paintObjects(Graphics2D g2) {

    for(int i=0;i < getNetObjectNumber();i++) {
      NetObject o = getNetObjectAt(i);
      switch(o.getUserType()) {
        case ITLK_BUBBLE:
          o.setCenterLabel(o.getExtendedParam(3 /* Type extensions */ ));
          String add = o.getExtendedParam(1) + "/" + o.getExtendedParam(2);
          o.setBottomLabel(add);
        break;
        case SENSOR_BUBBLE:
          o.setCenterLabel(o.getExtendedParam(0 /* Type extensions */ ));
          break;
      }
    }
    super.paintObjects(g2);

  }

  /** Apply the given device name to the whole selection */
  public void setDeviceName(String nDevice) {

    for(int i=0;i<getNetObjectNumber();i++) {
      NetObject o = getNetObjectAt(i);
      if(o.getUserType()==ITLK_BUBBLE && o.getSelected())
        o.setExtendedParam(0,nDevice);
    }

  }

}

