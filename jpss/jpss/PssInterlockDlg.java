package jpss;

import fr.esrf.tangoatk.widget.util.ATKConstant;
import fr.esrf.tangoatk.widget.util.interlock.NetObject;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.Tango.DevFailed;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A class to handle PSS interlock dialog.
 * User: Jean Luc Pons
 */
public class PssInterlockDlg extends JDialog implements ActionListener {

  private static Color fColor = new Color(99, 97, 156);

  JPanel innerPanel;

  JPanel idPanel;
  JLabel     devLabel;
  JTextField devName;
  JLabel     boardLabel;
  JTextField boardName;
  JLabel     relayLabel;
  JTextField relayName;

  JPanel     statePanel;
  JLabel     relayALabel;
  JTextField relayAValue;
  JLabel     relayBLabel;
  JTextField relayBValue;

  JPanel    descPanel;
  JTextArea descText;
  JScrollPane descView;
  JButton   applyDesc;
  JCheckBox smsCheck;

  JButton dismissBtn;

  DeviceProxy theDS;
  NetObject  theObj;

  public PssInterlockDlg(JFrame pFrame)  {

    super(pFrame,true);
    innerPanel = new JPanel();
    innerPanel.setLayout(null);

    idPanel=new JPanel();
    idPanel.setLayout(null);
    idPanel.setBorder(createTitleBorder("Identification"));
    idPanel.setBounds(10,10,380,115);

    devLabel = new JLabel("Device");
    devLabel.setFont(ATKConstant.labelFont);
    devLabel.setBounds(10,20,80,25);
    idPanel.add(devLabel);

    devName = new JTextField();
    devName.setEditable(false);
    devName.setBounds(100,20,370,25);
    idPanel.add(devName);

    boardLabel = new JLabel("Board");
    boardLabel.setFont(ATKConstant.labelFont);
    boardLabel.setBounds(10,50,80,25);
    idPanel.add(boardLabel);

    boardName = new JTextField();
    boardName.setEditable(false);
    boardName.setBounds(100,50,370,25);
    idPanel.add(boardName);

    relayLabel = new JLabel("Relay");
    relayLabel.setFont(ATKConstant.labelFont);
    relayLabel.setBounds(10,80,80,25);
    idPanel.add(relayLabel);

    relayName = new JTextField();
    relayName.setEditable(false);
    relayName.setBounds(100,80,370,25);
    idPanel.add(relayName);

    statePanel=new JPanel();
    statePanel.setLayout(null);
    statePanel.setBorder(createTitleBorder("Relay status"));
    statePanel.setBounds(10,130,380,85);

    relayALabel = new JLabel("Relay A");
    relayALabel.setFont(ATKConstant.labelFont);
    relayALabel.setBounds(10,20,80,25);
    statePanel.add(relayALabel);

    relayAValue = new JTextField();
    relayAValue.setEditable(false);
    relayAValue.setBounds(100,20,270,25);
    statePanel.add(relayAValue);

    relayBLabel = new JLabel("Relay B");
    relayBLabel.setFont(ATKConstant.labelFont);
    relayBLabel.setBounds(10,50,80,25);
    statePanel.add(relayBLabel);

    relayBValue = new JTextField();
    relayBValue.setEditable(false);
    relayBValue.setBounds(100,50,270,25);
    statePanel.add(relayBValue);

    descPanel=new JPanel();
    descPanel.setLayout(null);
    descPanel.setBorder(createTitleBorder("Settings"));
    descPanel.setBounds(10,225,380,160);

    descText = new JTextArea();
    descText.setEditable(true);
    descView = new JScrollPane(descText);
    descView.setBounds(10,50,360,70);
    descPanel.add(descView);

    smsCheck = new JCheckBox("Send Mail on alarm");
    smsCheck.addActionListener(this);
    smsCheck.setBounds(10, 20, 360, 25);
    smsCheck.setFont(ATKConstant.labelFont);
    descPanel.add(smsCheck);

    applyDesc = new JButton("Update descripton");
    applyDesc.addActionListener(this);
    applyDesc.setBounds(168,125,200,25);
    descPanel.add(applyDesc);

    dismissBtn = new JButton("Dismiss");
    dismissBtn.setBounds(287,390,100,25);
    dismissBtn.addActionListener(this);

    innerPanel.add(idPanel);
    innerPanel.add(statePanel);
    innerPanel.add(descPanel);
    innerPanel.add(dismissBtn);
    innerPanel.setPreferredSize(new Dimension(400,425));
    setContentPane(innerPanel);
    setTitle("PSS interlock");

  }

  public void setRelayState(JTextField txt,int state) {
    switch(state) {
      case PssNetEditor.CLOSE_STATE:
        txt.setText("Close");
        break;
      case PssNetEditor.OPEN_STATE:
        txt.setText("Open");
        break;
      case PssNetEditor.ALARM_STATE:
        break;
    }
  }

  public void showInterlockDialog(DeviceProxy ds,NetObject o) {

    theDS = ds;
    theObj = o;

    boardName.setText(o.getExtendedParam(1));
    relayName.setText(o.getExtendedParam(2));
    setRelayState(relayAValue, ((ItlkInfo)o.getUserValue()).stateA );
    setRelayState(relayBValue, ((ItlkInfo)o.getUserValue()).stateB );
    devName.setText(ds.get_name());

    try {

      DeviceData argin = new DeviceData();
      int[] add = new int[2];
      // PSS index start from 1
      add[0] = (Integer.parseInt(o.getExtendedParam(1)))-1;
      add[1] = (Integer.parseInt(o.getExtendedParam(2))-1)*2;
      argin.insert(add);
      DeviceData d = ds.command_inout("GetInterlockDescription",argin);
      descText.setText(d.extractStringArray()[0]);
      d = ds.command_inout("GetSMS",argin);
      smsCheck.setSelected(d.extractBoolean());

    } catch (DevFailed e) {

      JOptionPane.showMessageDialog(this, "GetInterlockDescription or GetSMS failed on" + ds.get_name()
              + "\n" + e.errors[0].desc);
      descText.setText("");

    }

    pack();
    centerWindow();
    show();
  }

  public void actionPerformed(ActionEvent evt) {

    Object src = evt.getSource();
    if( src == applyDesc ) {

      try {

        DeviceData argin = new DeviceData();
        String[] arg = new String[3];
        // PSS index start from 1
        arg[0] = Integer.toString(Integer.parseInt(theObj.getExtendedParam(1))-1);
        arg[1] = Integer.toString((Integer.parseInt(theObj.getExtendedParam(2))-1)*2);
        arg[2] = descText.getText();
        argin.insert(arg);
        theDS.command_inout("SetInterlockDescription",argin);

      } catch (DevFailed e) {

        JOptionPane.showMessageDialog(this, "SetInterlockDescription failed on" + theDS.get_name()
                + "\n" + e.errors[0].desc);

      }

    } else if ( src == smsCheck ) {

      try {

        DeviceData argin = new DeviceData();
        int[] arg = new int[2];
        // PSS index start from 1
        arg[0] =Integer.parseInt(theObj.getExtendedParam(1))-1;
        arg[1] =(Integer.parseInt(theObj.getExtendedParam(2)) - 1)*2;
        argin.insert(arg);
        if( smsCheck.isSelected() ) {
          theDS.command_inout("EnableSMS",argin);
        } else {
          theDS.command_inout("DisableSMS",argin);
        }

      } catch (DevFailed e) {

        JOptionPane.showMessageDialog(this, "EnableSMS or DisableSMS failed on" + theDS.get_name()
            + "\n" + e.errors[0].desc);

      }

    } else if ( src == dismissBtn ) {

      setVisible(false);

    }

  }

  static Border createTitleBorder(String name) {
    return BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), name, TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION, ATKConstant.labelFont, fColor);
  }

  private void centerWindow() {

    // Get the parent rectangle
    Rectangle r;
    if (getParent() != null) {
      r = getParent().getBounds();
    } else {
      Toolkit toolkit = Toolkit.getDefaultToolkit();
      Dimension d = toolkit.getScreenSize();
      r = new Rectangle(0, 0, d.width, d.height);
    }

    // Center
    int xe,ye,wx,wy;
    wx = getPreferredSize().width;
    wy = getPreferredSize().height;
    xe = r.x + (r.width - wx) / 2;
    ye = r.y + (r.height - wy) / 2;
    setBounds(xe, ye, wx, wy);

  }

}
