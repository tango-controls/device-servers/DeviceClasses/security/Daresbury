package jpss;

import org.tmatesoft.svn.core.*;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.wc.*;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Vector;

/**
 * A class to handle SVN command
 */

// CommitEventHandler --------------------------------------------------------------------------

class CommitEventHandler implements ISVNEventHandler {

  SVNHelper parent;

  CommitEventHandler(SVNHelper parent) {
    this.parent = parent;
  }

  public void handleEvent(SVNEvent event, double progress) {

    SVNEventAction action = event.getAction();

    if (action == SVNEventAction.COMMIT_MODIFIED) {
      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: Sending   " + event.getFile().getPath());
    } else if (action == SVNEventAction.COMMIT_DELETED) {
      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: Deleting   " + event.getFile().getPath());
    } else if (action == SVNEventAction.COMMIT_REPLACED) {
      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: Replacing   " + event.getFile().getPath());
    } else if (action == SVNEventAction.COMMIT_DELTA_SENT) {
      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: Transmitting file data....");
    } else if (action == SVNEventAction.COMMIT_ADDED) {

      // Gets the MIME-type of the item.
      String mimeType = event.getMimeType();
      if (SVNProperty.isBinaryMimeType(mimeType)) {
        /*
        * If the item is a binary file
        */
        if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: Adding  (bin)  " + event.getFile().getPath());
      } else {
        if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: Adding         " + event.getFile().getPath());
      }

    }

  }

  public void checkCancelled() throws SVNCancelException {
  }

}

// UpdateEventHandler --------------------------------------------------------------------------

class UpdateEventHandler implements ISVNEventHandler {

  SVNHelper parent;

  UpdateEventHandler(SVNHelper parent) {
    this.parent = parent;
  }

  public void handleEvent(SVNEvent event, double progress) {

    // Gets the current action. An action is represented by SVNEventAction.
    // In case of an update an  action  can  be  determined  via  comparing
    // SVNEvent.getAction() and SVNEventAction.UPDATE_-like constants.

    SVNEventAction action = event.getAction();
    String pathChangeType = " ";
    if (action == SVNEventAction.UPDATE_ADD) {

      // the item was added
      pathChangeType = "A";

    } else if (action == SVNEventAction.UPDATE_DELETE) {

      // the item was deleted
      pathChangeType = "D";

    } else if (action == SVNEventAction.UPDATE_UPDATE) {

      // Find out in details what  state the item is (after  having  been
      // updated).
      // Gets  the  status  of  file/directory  item   contents.  It   is
      // SVNStatusType  who contains information on the state of an item.
      SVNStatusType contentsStatus = event.getContentsStatus();
      if (contentsStatus == SVNStatusType.CHANGED) {
        // the  item  was  modified in the repository (got  the changes
        // from the repository
        pathChangeType = "U";
      } else if (contentsStatus == SVNStatusType.CONFLICTED) {
        // The file item is in  a  state  of Conflict. That is, changes
        // received from the repository during an update, overlap  with
        // local changes the user has in his working copy.
        pathChangeType = "C";
      } else if (contentsStatus == SVNStatusType.MERGED) {
        // The file item was merGed (those  changes that came from  the
        // repository  did  not  overlap local changes and were  merged
        // into the file).
        pathChangeType = "G";
      }

    } else if (action == SVNEventAction.UPDATE_EXTERNAL) {

      // for externals definitions
      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: Fetching external item into '" + event.getFile().getAbsolutePath() + "'");
      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: External at revision " + event.getRevision());
      return;

    } else if (action == SVNEventAction.UPDATE_COMPLETED) {

      // Working copy update is completed. Prints out the revision.
      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: At revision " + event.getRevision());
      return;

    } else if (action == SVNEventAction.ADD) {

      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: A     " + event.getFile().getPath());
      return;

    } else if (action == SVNEventAction.DELETE) {

      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: D     " + event.getFile().getPath());
      return;

    } else if (action == SVNEventAction.LOCKED) {

      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: L     " + event.getFile().getPath());
      return;

    } else if (action == SVNEventAction.LOCK_FAILED) {

      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: failed to lock    " + event.getFile().getPath());
      return;

    }

    // Status of properties of an item. SVNStatusType  also
    // contains information on the properties state.
    SVNStatusType propertiesStatus = event.getPropertiesStatus();
    String propertiesChangeType = " ";
    if (propertiesStatus == SVNStatusType.CHANGED) {

      // Properties were updated.
      propertiesChangeType = "U";

    } else if (propertiesStatus == SVNStatusType.CONFLICTED) {
      // Properties are in conflict with the repository.
      propertiesChangeType = "C";

    } else if (propertiesStatus == SVNStatusType.MERGED) {

      // Properties that came from the repository were  merged  with  the
      // local ones.
      propertiesChangeType = "G";

    }

    // Gets the status of the lock.
    String lockLabel = " ";
    SVNStatusType lockType = event.getLockStatus();

    if (lockType == SVNStatusType.LOCK_UNLOCKED) {

      // The lock is broken by someone.
      lockLabel = "B";

    }

    if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: " + pathChangeType + propertiesChangeType + lockLabel + "       " + event.getFile().getPath());

  }

  public void checkCancelled() throws SVNCancelException {
  }

}

// WCEventHandler --------------------------------------------------------------------------

class WCEventHandler implements ISVNEventHandler {

  SVNHelper parent;

  WCEventHandler(SVNHelper parent) {
    this.parent = parent;
  }

  public void handleEvent(SVNEvent event, double progress) {
    SVNEventAction action = event.getAction();

    if (action == SVNEventAction.ADD) {

      // The item is scheduled for addition.
      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: A     " + event.getFile().getPath());
      return;

    } else if (action == SVNEventAction.COPY) {

      // The  item  is  scheduled for addition  with history (copied,  in
      // other words).
      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: A  +  " + event.getFile().getPath());
      return;

    } else if (action == SVNEventAction.DELETE) {

      // The item is scheduled for deletion.
      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: D     " + event.getFile().getPath());
      return;

    } else if (action == SVNEventAction.LOCKED) {

      // The item is locked.
      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: L     " + event.getFile().getPath());
      return;

    } else if (action == SVNEventAction.LOCK_FAILED) {

      // Locking operation failed.
      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: failed to lock    " + event.getFile().getPath());
      return;

    }

  }

  public void checkCancelled() throws SVNCancelException {
  }

}

// StatusHandler --------------------------------------------------------------------------

class StatusHandler implements ISVNStatusHandler, ISVNEventHandler {

  SVNHelper parent;

  StatusHandler(SVNHelper parent) {
    this.parent = parent;
  }

  public void handleStatus(SVNStatus status) {

    // Gets  the  status  of  file/directory/symbolic link  text  contents.
    // It is  SVNStatusType  who  contains  information on the state of  an
    // item.
    SVNStatusType contentsStatus = status.getContentsStatus();

    String pathChangeType = " ";

    SVNStatusInfo statusInfo = new SVNStatusInfo();
    parent.retStatus = SVNHelper.SVN_STATUS_UP_TO_DATE;
    statusInfo.status = SVNHelper.SVN_STATUS_UP_TO_DATE;
    statusInfo.fileName = status.getFile().getName();

    boolean isAddedWithHistory = status.isCopied();
    if (contentsStatus == SVNStatusType.STATUS_MODIFIED) {
      // The contents of the file have been Modified.
      pathChangeType = "M";
      parent.retStatus = SVNHelper.SVN_STATUS_LOCALLY_MODIFIED;
      statusInfo.status = SVNHelper.SVN_STATUS_LOCALLY_MODIFIED;
    } else if (contentsStatus == SVNStatusType.STATUS_CONFLICTED) {
      // The item is in a state of Conflict.
      pathChangeType = "C";
      parent.retStatus = SVNHelper.SVN_STATUS_CONFLICT;
      statusInfo.status = SVNHelper.SVN_STATUS_CONFLICT;
    } else if (contentsStatus == SVNStatusType.STATUS_DELETED) {
      // The item has been scheduled for Deletion from the repository.
      pathChangeType = "D";
    } else if (contentsStatus == SVNStatusType.STATUS_ADDED) {
      // The item has been scheduled for Addition to the repository.
      pathChangeType = "A";
    } else if (contentsStatus == SVNStatusType.STATUS_UNVERSIONED) {
      // The item is not  under  version control.
      pathChangeType = "?";
    } else if (contentsStatus == SVNStatusType.STATUS_EXTERNAL) {
      // The item is unversioned, but is used by an eXternals definition.
      pathChangeType = "X";
    } else if (contentsStatus == SVNStatusType.STATUS_IGNORED) {
      // The item is Ignored.
      pathChangeType = "I";
    } else if (contentsStatus == SVNStatusType.STATUS_MISSING || contentsStatus == SVNStatusType.STATUS_INCOMPLETE) {
      // The file, directory or  symbolic  link  item  is  under  version
      // control but is missing or somehow incomplete.
      pathChangeType = "!";
    } else if (contentsStatus == SVNStatusType.STATUS_OBSTRUCTED) {
      // The item is in  the  repository as one kind of object,
      // but what's actually in the user's working
      // copy is some other kind.
      pathChangeType = "~";
    } else if (contentsStatus == SVNStatusType.STATUS_REPLACED) {
      // The item was  Replaced  in  the user's working copy; that is,
      // the item was deleted,  and  a  new item with the same name
      // was added (within  a  single  revision).
      pathChangeType = "R";
    } else if (contentsStatus == SVNStatusType.STATUS_NONE || contentsStatus == SVNStatusType.STATUS_NORMAL) {
      // The item was not modified (normal).
      pathChangeType = " ";
    }

    // If SVNStatusClient.doStatus(..) is invoked with  remote = true  the
    // following code finds out whether the current item has  been  changed
    // in the repository
    String remoteChangeType = " ";

    if (status.getRemotePropertiesStatus() != SVNStatusType.STATUS_NONE || status.getRemoteContentsStatus() != SVNStatusType.STATUS_NONE) {
      // the local item is out of date
      remoteChangeType = "*";
      if( parent.retStatus==SVNHelper.SVN_STATUS_LOCALLY_MODIFIED ) {
        parent.retStatus = SVNHelper.SVN_STATUS_OLD_MODIFIED;
        statusInfo.status = SVNHelper.SVN_STATUS_OLD_MODIFIED;
      } else {
        parent.retStatus = SVNHelper.SVN_STATUS_NEEDS_UPDATE;
        statusInfo.status = SVNHelper.SVN_STATUS_NEEDS_UPDATE;
      }
    }

    // Now getting the status of properties of an item. SVNStatusType  also
    // contains information on the properties state.
    SVNStatusType propertiesStatus = status.getPropertiesStatus();

    // Default - properties are normal (unmodified).
    String propertiesChangeType = " ";
    if (propertiesStatus == SVNStatusType.STATUS_MODIFIED) {
      // Properties were modified.
      propertiesChangeType = "M";
    } else if (propertiesStatus == SVNStatusType.STATUS_CONFLICTED) {
      // Properties are in conflict with the repository.
      propertiesChangeType = "C";
    }

    // Whether the item was locked in the .svn working area  (for  example,
    // during a commit or maybe the previous operation was interrupted, in
    // this case the lock needs to be cleaned up).
    boolean isLocked = status.isLocked();
    // Whether the item is switched to a different URL (branch).
    boolean isSwitched = status.isSwitched();
    // If the item is a file it may be locked.
    SVNLock localLock = status.getLocalLock();
    // If  doStatus()  was  run  with  remote = true  and the item is a file,
    // checks whether a remote lock presents.
    SVNLock remoteLock = status.getRemoteLock();
    String lockLabel = " ";

    if (localLock != null) {
      /*
      * at first suppose the file is locKed
      */
      lockLabel = "K";
      if (remoteLock != null) {
        // if the lock-token of the local lock differs from  the  lock-
        // token of the remote lock - the lock was sTolen!
        if (!remoteLock.getID().equals(localLock.getID())) {
          lockLabel = "T";
        }
      } else {
        lockLabel = "B";
      }
    } else if (remoteLock != null) {
      // the file is not locally locked but locked  in  the  repository -
      // the lock token is in some Other working copy.
      lockLabel = "O";
    }

    /*
    * Obtains the working revision number of the item.
    */
    long workingRevision = status.getRevision().getNumber();
    parent.retRevision = workingRevision;
    statusInfo.activeRev = workingRevision;

    /*
    * Obtains the number of the revision when the item was last changed.
    */
    long lastChangedRevision = status.getCommittedRevision().getNumber();
    statusInfo.lastChangedRev = lastChangedRevision;
    if(lastChangedRevision==-1) {
      parent.retStatus = SVNHelper.SVN_STATUS_NOT_CONTROLLED;
      statusInfo.status = SVNHelper.SVN_STATUS_NOT_CONTROLLED;
    }

    String offset = "                                ";
    String[] offsets = new String[3];
    offsets[0] = offset.substring(0, 6 - String.valueOf(workingRevision).length());
    offsets[1] = offset.substring(0, 6 - String.valueOf(lastChangedRevision).length());
    offsets[2] = offset.substring(0, offset.length() - (status.getAuthor() != null ? status.getAuthor().length() : 1));
    // status is shown in the manner of the native Subversion command  line
    // client's command "svn status"
    if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: " + pathChangeType
            + propertiesChangeType
            + (isLocked ? "L" : " ")
            + (isAddedWithHistory ? "+" : " ")
            + (isSwitched ? "S" : " ")
            + lockLabel
            + "  "
            + remoteChangeType
            + "  "
            + workingRevision
            + offsets[0]
            + (lastChangedRevision >= 0 ? String.valueOf(lastChangedRevision) : "?") + offsets[1]
            + (status.getAuthor() != null ? status.getAuthor() : "?")
            + offsets[2] + status.getFile().getName());

    parent.retNames.add(statusInfo);

  }

  public void handleEvent(SVNEvent event, double progress) {

    SVNEventAction action = event.getAction();
    // Print out the revision against which the status was performed.  This
    // event is dispatched when the SVNStatusClient.doStatus() was  invoked
    // with the flag remote set to true - that is for  a  local  status  it
    // won't be dispatched.
    if (action == SVNEventAction.STATUS_COMPLETED) {
      if(SVNHelper.PRINT_SVN_OUTPUT) System.out.println("SVN: Status against revision:  " + event.getRevision());
    }

  }

  public void checkCancelled() throws SVNCancelException {
  }
}

// LogHandler --------------------------------------------------------------------------

class LogHandler implements ISVNLogEntryHandler {

  SVNHelper parent;


  LogHandler(SVNHelper parent) {
    this.parent = parent;
  }

  public void handleLogEntry(SVNLogEntry logEntry) {

    SVNLogInfo info = new SVNLogInfo();

    info.revNumber = logEntry.getRevision();

    if( logEntry.getMessage().endsWith("\n") )
      info.logMessage = logEntry.getMessage().substring(0,logEntry.getMessage().length()-1);
    else
      info.logMessage = logEntry.getMessage();

    info.date = SVNHelper.dfr.format(logEntry.getDate());

    parent.retLog.add(info);

  }

}

// SVNHelper ---------------------------------------------------------------------------

public class SVNHelper {

  public final static boolean PRINT_SVN_OUTPUT = false;

  public final static int SVN_STATUS_UP_TO_DATE = 0;
  public final static int SVN_STATUS_LOCALLY_MODIFIED = 1;
  public final static int SVN_STATUS_NEEDS_UPDATE = 2;
  public final static int SVN_STATUS_CONFLICT = 3;
  public final static int SVN_STATUS_OLD_MODIFIED = 4;
  public final static int SVN_STATUS_NOT_CONTROLLED = 5;
  public final static int SVN_STATUS_UNKNOWN = 6;

  public static SimpleDateFormat dfr = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

  private SVNClientManager clientManager;
  private ISVNEventHandler myCommitEventHandler;
  private ISVNEventHandler myUpdateEventHandler;
  private ISVNEventHandler myWCEventHandler;
  private ISVNStatusHandler myStatusEventHandler;
  private ISVNLogEntryHandler myLogEventHandler;

  private SVNURL repositoryURL;

  // temp variable
  int retStatus;
  long retRevision;
  Vector<SVNLogInfo> retLog;
  Vector<SVNStatusInfo> retNames;

  public static String StatusToStr(int status) {

    switch(status) {
      case SVN_STATUS_UP_TO_DATE:
        return "Up to date";
      case SVN_STATUS_LOCALLY_MODIFIED:
        return "Locally modified";
      case SVN_STATUS_NEEDS_UPDATE:
        return "Needs update";
      case SVN_STATUS_CONFLICT:
        return "Conflicts";
      case SVN_STATUS_OLD_MODIFIED:
        return "Old revision modified";
      case SVN_STATUS_NOT_CONTROLLED:
        return "Not controlled";
      default:
        return "Unknown";
    }

  }

  // svn co
  public void checkout(String projectPath, SVNRevision revision, File destPath) throws SVNException {

    SVNURL url = repositoryURL.appendPath(projectPath, false);
    clientManager.getUpdateClient().doCheckout(url, destPath, revision, revision, SVNDepth.FILES, false);

  }

  // svn add
  public void add(File wcPath) throws SVNException {

    clientManager.getWCClient().doAdd(wcPath, false, false, false, SVNDepth.FILES, false, false, false);

  }

  // svn delete
  public void delete(File wcPath) throws SVNException {

    clientManager.getWCClient().doDelete(wcPath, true, true, false);

  }

  // svn update
  public void update(File wcPath, SVNRevision updateToRevision) throws SVNException {

    clientManager.getUpdateClient().doUpdate(wcPath, updateToRevision, SVNDepth.FILES, false, false);

  }

  // svn status (single file)
  public int status(File wcPath) throws SVNException {

    retStatus = SVN_STATUS_UNKNOWN;
    retNames.clear();
    clientManager.getStatusClient().doStatus(wcPath, SVNRevision.HEAD, SVNDepth.FILES, true, true, false, false, myStatusEventHandler, null);
    return retStatus;

  }

  // svn status (whole directory)
  public Vector<SVNStatusInfo> statusDir(File wcPath) throws SVNException {

    retNames.clear();
    clientManager.getStatusClient().doStatus(wcPath, SVNRevision.HEAD, SVNDepth.FILES, true, true, false, false, myStatusEventHandler, null);
    return retNames;

  }

  // svn status (returns active revision)
  public long getActiveRevision(File wcPath) throws SVNException {

    retRevision = -1;
    retNames.clear();
    clientManager.getStatusClient().doStatus(wcPath, SVNRevision.HEAD, SVNDepth.FILES, true, true, false, false, myStatusEventHandler, null);
    return retRevision;

  }

  // svn log (single file)
  public Vector<SVNLogInfo> log(File wcPath) throws SVNException {

    retLog.clear();
    SVNRevision startRev = SVNRevision.create(0);
    clientManager.getLogClient().doLog(new File[]{wcPath}, startRev, SVNRevision.HEAD, false, true, 100, myLogEventHandler);
    return retLog;

  }

  // svn commit
  public SVNCommitInfo commit(File wcPath, String commitMessage) throws SVNException {

    return clientManager.getCommitClient().doCommit(new File[]{wcPath}, true, commitMessage, null, null, false, true, SVNDepth.FILES);

  }

  // Construct a SVN helper
  public SVNHelper(String svnRoot) throws SVNException {

    FSRepositoryFactory.setup();
    repositoryURL = SVNURL.parseURIEncoded(svnRoot);

    // Creating custom handlers that will process events
    myCommitEventHandler = new CommitEventHandler(this);
    myUpdateEventHandler = new UpdateEventHandler(this);
    myWCEventHandler = new WCEventHandler(this);
    myStatusEventHandler = new StatusHandler(this);
    myLogEventHandler = new LogHandler(this);

    // Creates a default run-time configuration options driver. Default options
    // created in this way use the Subversion run-time configuration area (for
    // instance, on a Windows platform it can be found in the '%APPDATA%\Subversion'
    // directory).
    //
    // readonly = true - not to save  any configuration changes that can be done
    // during the program run to a config file (config settings will only
    // be read to initialize; to enable changes the readonly flag should be set
    // to false).
    //
    // SVNWCUtil is a utility class that creates a default options driver.

    ISVNOptions options = SVNWCUtil.createDefaultOptions(true);
    ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager();

    // Creates an instance of SVNClientManager providing a default auth
    // manager and an options driver
    clientManager = SVNClientManager.newInstance(options, authManager);

    // Registers event handlers
    clientManager.getCommitClient().setEventHandler(myCommitEventHandler);
    clientManager.getUpdateClient().setEventHandler(myUpdateEventHandler);
    clientManager.getWCClient().setEventHandler(myWCEventHandler);

    retLog = new Vector<SVNLogInfo>();
    retNames = new Vector<SVNStatusInfo>();

  }

}
