package jpss;

import fr.esrf.Tango.DevVarLongStringArray;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoDs.TangoConst;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * A class to handle interlock history selection.
 * User: Jean Luc Pons
 */
public class PssInterlockHistoryDlg extends JDialog implements ActionListener{

  private JPanel innerPanel;
  private JList theList;
  private JScrollPane theView;
  private JPanel buttonPanel;
  private JButton okButton;
  private JButton cancelButton;
  private int retValue;
  DevVarLongStringArray histories=null;

  static final java.text.SimpleDateFormat genFormat = new java.text.SimpleDateFormat("EEE dd/MM/yy HH:mm:ss");
  static final java.util.GregorianCalendar calendar = new java.util.GregorianCalendar();

  public PssInterlockHistoryDlg(JFrame pFrame)  {

    super(pFrame,true);

    innerPanel = new JPanel();
    innerPanel.setLayout(new BorderLayout());
    theList = new JList();
    theView = new JScrollPane(theList);
    theView.setBorder(BorderFactory.createEtchedBorder());
    innerPanel.add(theView,BorderLayout.CENTER);
    innerPanel.add(new JLabel("Available logs"),BorderLayout.NORTH);

    buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout());
    okButton = new JButton("Retrieve log");
    okButton.addActionListener(this);
    buttonPanel.add(okButton);
    cancelButton = new JButton("Dismiss");
    cancelButton.addActionListener(this);
    buttonPanel.add(cancelButton);
    innerPanel.add(buttonPanel,BorderLayout.SOUTH);

    setContentPane(innerPanel);
    setTitle("Select log");

  }

  // Show the log selection dialog and return the time value for the selected log. (in sec from Epoch)
  public int getHistoryTime(DeviceProxy itlkDevice) {

    retValue = -1;
    String[] dates;

    try {

      DeviceData hInfo = itlkDevice.command_inout("GetHistoryInfo");
      if( hInfo.getType() == TangoConst.Tango_DEVVAR_LONGSTRINGARRAY ) {
        histories = hInfo.extractLongStringArray();
      } else {
        histories = new DevVarLongStringArray();
        histories.lvalue = hInfo.extractLongArray();
        histories.svalue = new String[histories.lvalue.length];
        for(int i=0;i<histories.svalue.length;i++) {
          histories.svalue[i] = "";
        }
      }

      dates = new String[histories.lvalue.length];
      for (int i = 0; i < histories.lvalue.length; i++)
        dates[i] = formatTimeValue(histories.lvalue[i]) + "[" + histories.svalue[i] + "]";
      theList.setListData(dates);

      pack();
      centerWindow();
      show();

    } catch (DevFailed e) {

      JOptionPane.showMessageDialog(this, "Error during GetHistoryInfo on:" + itlkDevice.get_name() + "\n" + e.errors[0].desc);

    }

    return retValue;

  }

  public static String formatTimeValue(int vt) {
      java.util.Date date;
      calendar.setTimeInMillis(((long)vt)*1000L);
      date = calendar.getTime();
      return genFormat.format(date);
  }

  // -----------------------------------------------------
  // Action Listener
  // -----------------------------------------------------
  public void actionPerformed(java.awt.event.ActionEvent evt) {

    Object src = evt.getSource();

    if (src == okButton) {

      retValue=histories.lvalue[theList.getSelectedIndex()];
      hide();
      dispose();

    } else if (src == cancelButton) {

      hide();
      dispose();

    }

  }

  private void centerWindow() {

    // Get the parent rectangle
    Rectangle r;
    if (getParent() != null) {
      r = getParent().getBounds();
    } else {
      Toolkit toolkit = Toolkit.getDefaultToolkit();
      Dimension d = toolkit.getScreenSize();
      r = new Rectangle(0, 0, d.width, d.height);
    }

    // Center
    int xe,ye,wx,wy;
    wx = getPreferredSize().width;
    wy = getPreferredSize().height;
    xe = r.x + (r.width - wx) / 2;
    ye = r.y + (r.height - wy) / 2;
    setBounds(xe, ye, wx, wy);

  }

}
