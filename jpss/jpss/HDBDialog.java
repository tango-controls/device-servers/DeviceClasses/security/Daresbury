package jpss;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Vector;
import org.tango.jhdb.Hdb;
import org.tango.jhdb.HdbFailed;
import org.tango.jhdb.data.HdbData;
import org.tango.jhdb.data.HdbDataSet;

import static jpss.PssConst.TANGO_HOST;

class ItlkHDBItem {

  ItlkHDBItem(String name, long date, long[] value) {
    this.devName = name;
    this.date = date;
    this.value = value;
  }

  long date;
  long[] value;
  String devName;

}

/**
 * Display HDB dialog
 */
public class HDBDialog extends JDialog implements ActionListener {

  public final static SimpleDateFormat dfr = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

  private String devName;
  private String attName = "interlocks";
  private Vector<ItlkHDBItem> hdbData;

  private JPanel innerPanel;
  private JPanel configPanel;
  private JPanel buttonPanel;

  private DefaultTableModel dm;
  private JTable theTable;

  private JComboBox timeCombo;
  private JLabel startDateLabel;
  private JTextField startDateText;
  private JLabel stopDateLabel;
  private JTextField stopDateText;
  private JCheckBox removeDuplicateCheck;
  private JButton performSearchBtn;
  private JButton dismissBtn;
  private JButton selectBtn;
  private Hdb hdb;

  private String colName[] = new String[23];

  public boolean selectOK;
  public short[] retItlk;
  public int retTimeStamp;


  public HDBDialog(JFrame pFrame) {

    super(pFrame,true);

    hdbData = new Vector<ItlkHDBItem>();
    colName[0] = "Date";
    for(int i=1;i<=22;i++) colName[i] = String.format("M%02d",i);

    innerPanel = new JPanel();
    innerPanel.setLayout(new BorderLayout());
    setContentPane(innerPanel);

    configPanel = new JPanel();
    configPanel.setLayout(null);
    configPanel.setPreferredSize(new Dimension(1300, 120));
    configPanel.setBorder(PssInterlockDlg.createTitleBorder("HDB request"));
    innerPanel.add(configPanel, BorderLayout.NORTH);


    timeCombo = new JComboBox();
    timeCombo.addItem("Last 4 Hours");
    timeCombo.addItem("Last 8 Hours");
    timeCombo.addItem("Last 24 Hours");
    timeCombo.addItem("Last 3 days");
    timeCombo.addItem("Last week");
    timeCombo.setBounds(10, 20, 300, 25);
    timeCombo.addActionListener(this);
    configPanel.add(timeCombo);

    startDateLabel = new JLabel("Start date");
    startDateLabel.setBounds(10, 50, 100, 25);
    configPanel.add(startDateLabel);
    startDateText = new JTextField();
    startDateText.setEditable(true);
    startDateText.setBounds(110, 50, 200, 25);
    configPanel.add(startDateText);

    stopDateLabel = new JLabel("Stop date");
    stopDateLabel.setBounds(10, 80, 100, 25);
    configPanel.add(stopDateLabel);
    stopDateText = new JTextField();
    stopDateText.setEditable(true);
    stopDateText.setBounds(110, 80, 200, 25);
    configPanel.add(stopDateText);

    removeDuplicateCheck = new JCheckBox("Remove duplicate data");
    removeDuplicateCheck.setBounds(320, 50, 250, 25);
    removeDuplicateCheck.setSelected(true);
    configPanel.add(removeDuplicateCheck);

    performSearchBtn = new JButton("Perform search");
    performSearchBtn.addActionListener(this);
    performSearchBtn.setBounds(320, 80, 150, 25);
    configPanel.add(performSearchBtn);

    buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    innerPanel.add(buttonPanel, BorderLayout.SOUTH);


    selectBtn = new JButton("Select");
    selectBtn.addActionListener(this);
    buttonPanel.add(selectBtn);

    dismissBtn = new JButton("Dismiss");
    dismissBtn.addActionListener(this);
    buttonPanel.add(dismissBtn);

    // Table model
    dm = new DefaultTableModel() {

      public Class getColumnClass(int columnIndex) {
        if(columnIndex==0) return String.class;
        else return Long.class;
      }

      public boolean isCellEditable(int row, int column) {
        return false;
      }

      public void setValueAt(Object aValue, int row, int column) {
      }

    };

    // Table initialisation
    theTable = new JTable(dm);
    ItlkCellRenderer cellRenderer = new ItlkCellRenderer();
    theTable.setDefaultRenderer(Long.class,cellRenderer);

    theTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    theTable.setRowSelectionAllowed(true);

    JScrollPane scrollPane = new JScrollPane(theTable);
    innerPanel.add(scrollPane, BorderLayout.CENTER);

    clear();

    timeCombo.setSelectedIndex(0);

    hdb = new Hdb();
    try {
      //export HDB_NAME=hdb
      //export HDB_TYPE=postgresql
      //export HDB_USER=hdb_java_reporter
      //export HDB_PASSWORD=hdbpp
      //export HDB_POSTGRESQL_HOST=hdb-services
      //export HDB_POSTGRESQL_PORT=5001

      // All the above environment variables should be set inside the shell to be able to connect
      // Connect to HDB ++ using system variables
      hdb.connect();
      System.out.println("HDB connection Ok: " + hdb.getReader().getInfo());
    } catch (HdbFailed ex) {
      JOptionPane.showMessageDialog(null, ex.getMessage(), "HDB Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

  }

  public void setDevName(String name) {
    if(devName==null || !devName.equalsIgnoreCase(name)) {
      clear();
      devName = name;
      setTitle("HDB [" + devName + "/" + attName + "]");
    }
  }

  public void clear() {

    hdbData.clear();
    String[][] prop = new String[1][23];
    for(int i=0;i<23;i++) prop[0][i] = "";
    dm.setDataVector(prop, colName);
    theTable.getColumnModel().getColumn(0).setPreferredWidth(300);
    theTable.validate();

  }

  private void addHDBData(String feName, long date, long[] value) {

    // Search insertion position
    boolean found = false;
    boolean addData = true;
    int i = 0;
    while (i < hdbData.size() && !found) {
      found = date < hdbData.get(i).date;
      if (!found) {
        i++;
      }
    }

    if (removeDuplicateCheck.isSelected()) {
      if (i > 0) {
        ItlkHDBItem prec = hdbData.get(i - 1);
        addData = !Arrays.equals(prec.value,value);
      }
    }

    // Add new item
    if (addData) {

      ItlkHDBItem it = new ItlkHDBItem(feName, date, value);

      if (!found) {
        hdbData.add(it);
      } else {
        hdbData.add(i, it);
      }

    }

  }

  private void HDBSearchSingle() {

    // Check parameters
    String d1 = startDateText.getText();
    java.util.Date d;
    try {
      d = dfr.parse(d1);
    } catch (java.text.ParseException e1) {
      JOptionPane.showMessageDialog(null, "Invalid start date format\nUse dd/mm/yyyy hh:mm:ss",
              "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    String d2 = stopDateText.getText();
    try {
      d = dfr.parse(d2);
    } catch (java.text.ParseException e2) {
      JOptionPane.showMessageDialog(null, "Invalid stop date format\nUse dd/mm/yyyy hh:mm:ss",
              "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    hdbData.clear();

    HdbDataSet data;

    String fullName = TANGO_HOST + devName + "/" + attName;
    try {
      data = hdb.getReader().getData(fullName, startDateText.getText(), stopDateText.getText());
    } catch (HdbFailed ex) {
      JOptionPane.showMessageDialog(getParent(), fullName + "\n" + ex.getMessage(), "HDB Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    int dataSize = data.size();

    if (dataSize > 0) {
      for (int j = 0; j < dataSize; j++) {
        HdbData dataj = data.get(j);
        try {
          addHDBData(devName, dataj.getDataTime(), dataj.getValueAsLongArray());
        } catch (HdbFailed ex) {
          //JOptionPane.showMessageDialog(getParent(), ex.getMessage(), "HDB Error", JOptionPane.ERROR_MESSAGE);
        }
      }
    }

    if (hdbData.size() == 0) {

      JOptionPane.showMessageDialog(null, "No data found", "HDB Error", JOptionPane.ERROR_MESSAGE);

    } else {

      // Build the table
      Object[][] vals = new Object[hdbData.size()][23];
      for (int i = 0; i < hdbData.size(); i++) {
        Date dt = new java.util.Date(hdbData.get(i).date / 1000);
        vals[i][0] = dfr.format(dt);
        if(hdbData.get(i).value.length!=22) {
          for (int j = 0; j < 22; j++)
            vals[i][j + 1] = "";
        } else {
          for (int j = 0; j < 22; j++)
            vals[i][j + 1] = hdbData.get(i).value[j];
        }
      }

      dm.setDataVector(vals, colName);
      theTable.getColumnModel().getColumn(0).setPreferredWidth(300);
      theTable.validate();

    }

  }

  public void actionPerformed(ActionEvent e) {

    Object src = e.getSource();

    if (src == timeCombo) {

      long now = System.currentTimeMillis();

      switch (timeCombo.getSelectedIndex()) {
        case 0: //Last 4 Hours
        {
          Date dstop = new java.util.Date(now);
          stopDateText.setText(dfr.format(dstop));
          Date dstart = new java.util.Date(now - 14400000);
          startDateText.setText(dfr.format(dstart));
        }
        break;
        case 1: //Last 8 Hours
        {
          Date dstop = new java.util.Date(now);
          stopDateText.setText(dfr.format(dstop));
          Date dstart = new java.util.Date(now - 28800000);
          startDateText.setText(dfr.format(dstart));
        }
        break;
        case 2: //Last 24 Hours
        {
          Date dstop = new java.util.Date(now);
          stopDateText.setText(dfr.format(dstop));
          Date dstart = new java.util.Date(now - 86400000);
          startDateText.setText(dfr.format(dstart));
        }
        break;
        case 3: //Last 3 days
        {
          Date dstop = new java.util.Date(now);
          stopDateText.setText(dfr.format(dstop));
          Date dstart = new java.util.Date(now - 259200000);
          startDateText.setText(dfr.format(dstart));
        }
        break;
        case 4: // Last week
        {
          Date dstop = new java.util.Date(now);
          stopDateText.setText(dfr.format(dstop));
          Date dstart = new java.util.Date(now - 604800000);
          startDateText.setText(dfr.format(dstart));
        }
        break;
      }

    } else if (src == dismissBtn) {

      selectOK = false;
      retItlk = null;
      retTimeStamp = -1;
      setVisible(false);

    } else if (src == selectBtn) {

      int row = theTable.getSelectedRow();
      if(row<0) {
        JOptionPane.showMessageDialog(null, "No row selected", "HDB Error", JOptionPane.ERROR_MESSAGE);
      } else if (row>=hdbData.size() || hdbData.get(row).value.length!=22) {
        JOptionPane.showMessageDialog(null, "Invalid data selection", "HDB Error", JOptionPane.ERROR_MESSAGE);
      } else {
        selectOK = true;
        retItlk = new short[22];
        for(int i=0;i<22;i++) retItlk[i] = (short)hdbData.get(row).value[i];
        retTimeStamp = (int)(hdbData.get(row).date / 1000000);
        setVisible(false);
      }

    } else if (src == performSearchBtn) {

      HDBSearchSingle();

    }

  }

}