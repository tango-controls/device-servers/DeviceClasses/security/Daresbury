//=============================================================================
//
// file :        MailSender.h
//
// description : Include file for the MailSender class
//
// project :     Daresbury
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author: pons $
//
// $Revision: 1.1.1.1 $
// $Date: 2013/12/11 13:44:48 $
//
// $Log: MailSender.h,v $
// Revision 1.1.1.1  2013/12/11 13:44:48  pons
// Initial releasae
//
//
//=============================================================================

#ifndef MAILSENDERH
#define MAILSENDERH

#include <string>
#include <vector>

class MailSender {

  public:

    MailSender(std::string SMTPServerName);
    bool SendMessage(std::vector<std::string> destAdd,std::string fromAdd,std::string subject,std::string message);
    std::string GetLastError();

  private:

    int Write(int sock, char *buf, int bufsize,int timeout); // Timeout in millisec
    int Read(int sock, char *buf, int bufsize,int timeout);
    int WaitFor(int sock,int timeout,int mode);
    bool CheckResponse(char *cmd,int sock,int expectedCode);

    std::string SMTPServer;    
    std::string lastError;

};

#endif /* MAILSENDERH */
