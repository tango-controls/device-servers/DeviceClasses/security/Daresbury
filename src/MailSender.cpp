//=============================================================================
//
// file :        MailSender.cpp
//
// description : Send EMail using SMTP protocol
//
// project :     Daresbury
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author: pons $
//
// $Revision: 1.1.1.1 $
// $Date: 2013/12/11 13:44:48 $
//
// $Log: MailSender.cpp,v $
// Revision 1.1.1.1  2013/12/11 13:44:48  pons
// Initial releasae
//
//
//=============================================================================

#include "MailSender.h"
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>

#define STR(x)          (char *)(x)

using namespace std;

#define WAIT_FOR_READ  1
#define WAIT_FOR_WRITE 2

#define COMM_TIMEOUT 100 // 100msec timeout

#define closesocket close

// ----------------------------------------------------------------------------

MailSender::MailSender(std::string SMTPServerName) {

  SMTPServer = SMTPServerName;
  lastError = "";
  
}

// -------------------------------------------------------

int MailSender::WaitFor(int sock,int timeout,int mode) {

  fd_set fdset;
  fd_set *rd = NULL, *wr = NULL;
  struct timeval tmout;
  int result;

  FD_ZERO (&fdset);
  FD_SET (sock, &fdset);
  if (mode == WAIT_FOR_READ)
    rd = &fdset;
  if (mode == WAIT_FOR_WRITE)
    wr = &fdset;

  tmout.tv_sec  = (int)(timeout / 1000);
  tmout.tv_usec = (int)(timeout % 1000) * 1000;

  do
    result = select (sock + 1, rd, wr, NULL, &tmout);
  while (result < 0 && errno == EINTR);

  if( result==0 ) {
    lastError = "SMTP: The operation timed out";
  } else if ( result < 0 ) {
    lastError = "SMTP: Tranmission error";
    return 0;
  }

  return result;

}

// ----------------------------------------------------------------------------

int MailSender::Write(int sock, char *buf, int bufsize,int timeout) { // Timeout in millisec

  int total_written = 0;
  int written = 0;

  while( bufsize > 0 )
  {
    // Wait
    if (!WaitFor(sock, timeout, WAIT_FOR_WRITE))
      return -1;

    // Write
    do
      written = send(sock, buf, bufsize, 0);
    while (written == -1 && errno == EINTR);

    if( written <= 0 )
       break;

    buf += written;
    total_written += written;
    bufsize -= written;
  }

  if( written < 0 ) {
    lastError = "SMTP: Tranmission error";
    return -1;
  }

  return total_written;

}

// ----------------------------------------------------------------------------

int MailSender::Read(int sock, char *buf, int bufsize,int timeout) { // Timeout in millisec

  int rd = 0;
  int total_read = 0;  

//  while( bufsize>0 ) {

    // Wait
    if (!WaitFor(sock, timeout, WAIT_FOR_READ)) {
      return -1;
    }

    // Read
    do
      rd = recv(sock, buf, bufsize, 0);
    while (rd == -1 && errno == EINTR);

//    if( rd <= 0 )
//      break;

    buf += rd;
    total_read += rd;
    bufsize -= rd;
  
//  }

  if( rd < 0 ) {
    lastError = "SMTP: Tranmission error";
    return -1;
  }
  
  return total_read;

}

// ----------------------------------------------------------------------------

bool MailSender::CheckResponse(char *cmd,int sock,int expectedCode) {

  char buffer[1024];
  int nbRead;

  nbRead = Read(sock,buffer,4096,COMM_TIMEOUT);
  if( nbRead<0 ) {
    closesocket(sock);
    return false;
  }

  if( nbRead<3 ) {
    closesocket(sock);
    lastError = "SMTP: Unexpected SMTP response";
    return false;
  }  
  
  buffer[3]=0;

  int retCode = atoi(buffer);

  if( retCode != expectedCode ) {
    closesocket(sock);
    char tmp[256];
    sprintf(tmp,"%d",expectedCode);
    lastError = "SMTP: Unexpected SMTP response for " + string(cmd) + 
                ", got:" + string(buffer) + " ,expected:" + string(tmp);
    return false;
  }

  return true;

}

// ----------------------------------------------------------------------------

bool MailSender::SendMessage(vector<string> destAdd,string fromAdd,string subject,string message) {

  struct hostent *host_info;
  struct sockaddr_in server;
  char buffer[4096];
  int nbRead;
  int nbSent;

  // Resolve IP
  host_info = gethostbyname(SMTPServer.c_str());
  if (host_info == NULL) {  
     lastError = "Unknown host: " + SMTPServer;
     return false; 
  }

  // Build TCP connection
  int sock = socket(AF_INET, SOCK_STREAM,0);
  if (sock < 0 ) {
    switch(errno) {
      case EACCES:
        lastError = "SMTP: Socket error: Permission denied";
        break;
      case EMFILE:
        lastError = "SMTP: Socket error: Descriptor table is full";
        break;
      case ENOMEM:
        lastError = "SMTP: Socket error: Insufficient user memory is available";
        break;
      case ENOSR:
        lastError = "SMTP: Socket error: Insufficient STREAMS resources available";
        break;
      case EPROTONOSUPPORT:
        lastError = "SMTP: Socket error: Specified protocol is not supported";
        break;
      default:
        lastError = "SMTP: Socket error: Code:" + errno;
        break;
    }
    return false;
  }  

  // Connect
  memset(&server,0,sizeof(sockaddr_in));
  server.sin_family = host_info->h_addrtype;
  memcpy((char*)&server.sin_addr, host_info->h_addr,host_info->h_length);
  server.sin_port=htons(25);
  if( connect(sock,(struct sockaddr *)&server, sizeof(server) ) < 0 ) {
    lastError = "Cannot connect to host: " + SMTPServer;
    closesocket(sock);
    return false;
  }

  // Read header message
  if( !CheckResponse(STR("HEADER"),sock,220) ) return false;

  // Send HELO message
  sprintf(buffer,"HELO dserver\n");
  nbSent = Write(sock,buffer,strlen(buffer),COMM_TIMEOUT);
  if( nbSent!=(int)strlen(buffer) ) {
    closesocket(sock);
    return false;
  }
  if(!CheckResponse(STR("HELO"),sock,250)) return false;

  // Send MAIL FROM
  sprintf(buffer,"MAIL FROM: <%s>\n" , fromAdd.c_str());
  nbSent = Write(sock,buffer,strlen(buffer),COMM_TIMEOUT);
  if( nbSent!=(int)strlen(buffer) ) {
    closesocket(sock);
    return false;
  }
  if(!CheckResponse(STR("MAIL FROM"),sock,250)) return false;

  // Send RCPT TO
  for(int i=0;i<(int)destAdd.size();i++) {
  
    sprintf(buffer,"RCPT TO: <%s>\n" , destAdd[i].c_str());
    nbSent = Write(sock,buffer,strlen(buffer),COMM_TIMEOUT);
    if( nbSent!=(int)strlen(buffer) ) {
      closesocket(sock);
      return false;
    }
    if(!CheckResponse(STR("RCPT TO"),sock,250)) return false;
    
  }

  // Send DATA
  strcpy(buffer,"DATA\n");
  nbSent = Write(sock,buffer,strlen(buffer),COMM_TIMEOUT);
  if( nbSent!=(int)strlen(buffer) ) {
    closesocket(sock);
    return false;
  }
  if(!CheckResponse(STR("DATA"),sock,354)) return false;

  // Send Message
  sprintf(buffer,"Subject: %s\n\n%s\n.\n",subject.c_str(),message.c_str());
  nbSent = Write(sock,buffer,strlen(buffer),COMM_TIMEOUT);
  if( nbSent!=(int)strlen(buffer) ) {
    closesocket(sock);
    return false;
  }
  if(!CheckResponse(STR("BODY"),sock,250)) return false;

  // Send QUIT
  strcpy(buffer,"QUIT\n");
  nbSent = Write(sock,buffer,strlen(buffer),COMM_TIMEOUT);
  if( nbSent!=(int)strlen(buffer) ) {
    closesocket(sock);
    return false;
  }
  if(!CheckResponse(STR("QUIT"),sock,221)) return false;

  closesocket(sock);
  return true;

}

// ----------------------------------------------------------------------------

string MailSender::GetLastError() {

  return lastError;

}
