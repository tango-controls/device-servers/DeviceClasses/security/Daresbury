/*----- PROTECTED REGION ID(DaresburyStateMachine.cpp) ENABLED START -----*/
static const char *RcsId = "$Id:  $";
//=============================================================================
//
// file :        DaresburyStateMachine.cpp
//
// description : State machine file for the Daresbury class
//
// project :     Daresbury
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <Daresbury.h>

/*----- PROTECTED REGION END -----*/	//	Daresbury::DaresburyStateMachine.cpp

//================================================================
//  States  |  Description
//================================================================


namespace Daresbury_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method     : Daresbury::is_Interlocks_allowed()
 *	Description: Execution allowed for Interlocks attribute
 */
//--------------------------------------------------------
bool Daresbury::is_Interlocks_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for Interlocks attribute in read access.
	/*----- PROTECTED REGION ID(Daresbury::InterlocksStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Daresbury::InterlocksStateAllowed_READ
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method     : Daresbury::is_DevRead_allowed()
 *	Description: Execution allowed for DevRead attribute
 */
//--------------------------------------------------------
bool Daresbury::is_DevRead_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for DevRead command.
	/*----- PROTECTED REGION ID(Daresbury::DevReadStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Daresbury::DevReadStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method     : Daresbury::is_GetAllDescription_allowed()
 *	Description: Execution allowed for GetAllDescription attribute
 */
//--------------------------------------------------------
bool Daresbury::is_GetAllDescription_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for GetAllDescription command.
	/*----- PROTECTED REGION ID(Daresbury::GetAllDescriptionStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Daresbury::GetAllDescriptionStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method     : Daresbury::is_GetHistoryInfo_allowed()
 *	Description: Execution allowed for GetHistoryInfo attribute
 */
//--------------------------------------------------------
bool Daresbury::is_GetHistoryInfo_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for GetHistoryInfo command.
	/*----- PROTECTED REGION ID(Daresbury::GetHistoryInfoStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Daresbury::GetHistoryInfoStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method     : Daresbury::is_GetInterlockDescription_allowed()
 *	Description: Execution allowed for GetInterlockDescription attribute
 */
//--------------------------------------------------------
bool Daresbury::is_GetInterlockDescription_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for GetInterlockDescription command.
	/*----- PROTECTED REGION ID(Daresbury::GetInterlockDescriptionStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Daresbury::GetInterlockDescriptionStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method     : Daresbury::is_GetInterlockState_allowed()
 *	Description: Execution allowed for GetInterlockState attribute
 */
//--------------------------------------------------------
bool Daresbury::is_GetInterlockState_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for GetInterlockState command.
	/*----- PROTECTED REGION ID(Daresbury::GetInterlockStateStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Daresbury::GetInterlockStateStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method     : Daresbury::is_ReadInterlockHistory_allowed()
 *	Description: Execution allowed for ReadInterlockHistory attribute
 */
//--------------------------------------------------------
bool Daresbury::is_ReadInterlockHistory_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for ReadInterlockHistory command.
	/*----- PROTECTED REGION ID(Daresbury::ReadInterlockHistoryStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Daresbury::ReadInterlockHistoryStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method     : Daresbury::is_Reset_allowed()
 *	Description: Execution allowed for Reset attribute
 */
//--------------------------------------------------------
bool Daresbury::is_Reset_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Reset command.
	/*----- PROTECTED REGION ID(Daresbury::ResetStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Daresbury::ResetStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method     : Daresbury::is_SetInterlockDescription_allowed()
 *	Description: Execution allowed for SetInterlockDescription attribute
 */
//--------------------------------------------------------
bool Daresbury::is_SetInterlockDescription_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for SetInterlockDescription command.
	/*----- PROTECTED REGION ID(Daresbury::SetInterlockDescriptionStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Daresbury::SetInterlockDescriptionStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method     : Daresbury::is_EnableSMS_allowed()
 *	Description: Execution allowed for EnableSMS attribute
 */
//--------------------------------------------------------
bool Daresbury::is_EnableSMS_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for EnableSMS command.
	/*----- PROTECTED REGION ID(Daresbury::EnableSMSStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Daresbury::EnableSMSStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method     : Daresbury::is_DisableSMS_allowed()
 *	Description: Execution allowed for DisableSMS attribute
 */
//--------------------------------------------------------
bool Daresbury::is_DisableSMS_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for DisableSMS command.
	/*----- PROTECTED REGION ID(Daresbury::DisableSMSStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Daresbury::DisableSMSStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method     : Daresbury::is_GetSMS_allowed()
 *	Description: Execution allowed for GetSMS attribute
 */
//--------------------------------------------------------
bool Daresbury::is_GetSMS_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for GetSMS command.
	/*----- PROTECTED REGION ID(Daresbury::GetSMSStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Daresbury::GetSMSStateAllowed
	return true;
}


/*----- PROTECTED REGION ID(Daresbury::DaresburyStateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	Daresbury::DaresburyStateAllowed.AdditionalMethods

}	//	End of namespace
